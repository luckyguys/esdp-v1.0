package TaskManager;

import java.applet.AppletContext;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.StringTokenizer;
import java.util.TimeZone;
import java.util.Vector;

import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.TitledBorder;
import javax.swing.event.HyperlinkListener;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

public class Url extends JFrame implements ActionListener {
	public Container con_Url = this.getContentPane();
	private static final String IMAGE_PATH = "resource/Image/";  //이미지 위치
	private static final String FILE_PATH = "resource/file/";  //파일 저장 위치
	private static final String URL_PATH = "resource/url/";  //이미지 위치
	private JDialog dig_Write = new JDialog(this, "즐겨찾기", true);
	
	// 스케쥴 패널
	
	//

	private JPanel panel_total = new JPanel();
	private JPanel left_Panel = new JPanel();
	private JPanel rightdown_Panel = new JPanel();
	private JTextField tf_Schdule = new JTextField(37);
	
	private JTextArea ta_Memo = new JTextArea();
	
	private JScrollPane sp_Memo = new JScrollPane(ta_Memo);
	private JTextField url_field = new JTextField(37);
	
	private JButton bt_save = new JButton("등            록");
	private JButton bt_cancel = new JButton("취            소");
	private JButton excute_bt = new JButton("실 행");
	
	private JButton bt_add = new JButton("추 가");
	private JButton bt_modify = new JButton("수 정");
	private JButton bt_del = new JButton("삭 제");
	
	
	
	// 스케쥴 추가 수정
	private String[] info_temp = new String[3];
	private String schdule, memo, address;
	private int snum;
	
	//리스트출력
	private String[] str = {"이름", "내용", "주소"};  
	private DefaultTableModel dtm = new DefaultTableModel(str, 0){
		boolean[] canEdit = new boolean[]{false , false, false, false, false, false, false};
		public boolean isCellEditable(int rowIndex, int columnIndex){
			return canEdit[columnIndex];
		}
	};
	private DefaultTableColumnModel dtcm = new DefaultTableColumnModel(); //열너비 조절위해
	private JTable table = new JTable(dtm, dtcm);
	private JScrollPane jsp = new JScrollPane(table);
	private TableColumn tc1, tc2, tc3; //열 너비 지정

	private Dimension screen;
	private int xpos, ypos;
	
	private Vector vData = new Vector(); 
	
	public Url(){
		init();
		left();
		right();
		list();
		write();
		action();
		load();
		screen = Toolkit.getDefaultToolkit().getScreenSize();
		xpos = (int)(screen.getWidth() / 2 - this.getWidth() / 2);
		ypos = (int)(screen.getHeight() / 2 - this.getHeight() / 2);
	}
	public void init(){
		//con_sched.add("Center", rightup_Panel);
		//con_sched.add("West", left_Panel);
		con_Url.add(panel_total);
		
	}

	public void left()
	{
		
		left_Panel.setBorder(new TitledBorder("이름 및 내용"));
		left_Panel.setLayout(new BorderLayout());
		
		JPanel left_title = new JPanel();
		JPanel subPanel = new JPanel();
		JPanel left_text = new JPanel();
		
		//나열하기
		subPanel.setLayout(new BorderLayout());
		
		left_title.setBorder(new TitledBorder("이       름"));
		left_title.add(tf_Schdule);
		
		sp_Memo.setWheelScrollingEnabled(true);
		sp_Memo.setAutoscrolls(true);
		sp_Memo.setPreferredSize(new Dimension(410, 72));
		left_text.setBorder(new TitledBorder("내       용"));
		left_text.add(sp_Memo);
		
		
		subPanel.add("North", left_title);
		subPanel.add("South", left_text);
		
		left_Panel.add(subPanel);
	}
	public void right()
	{
		//rightdown_Panel.setBorder(new TitledBorder("URL"));
		rightdown_Panel.setLayout(new BorderLayout());
		
		JPanel url_panel = new JPanel();
		JPanel button_panel = new JPanel();

				
		bt_save.setPreferredSize(new Dimension(200, 23 ));
		bt_cancel.setPreferredSize(new Dimension(200, 23 ));
		
		url_panel.setBorder(new TitledBorder("URL"));
		url_panel.setLayout(new BorderLayout());
		url_panel.add("Center", url_field);
		//url_panel.add("East", excute_bt);
		
		button_panel.setBorder(new TitledBorder(""));
		button_panel.setLayout(new GridLayout(1,2));
		button_panel.add(bt_save);
		//button_panel.add(bt_cancel);
		
		rightdown_Panel.add("South", button_panel);
		rightdown_Panel.add("North", url_panel);
		
	}
	
	public void write()
	{
		JPanel total_write = new JPanel();
		total_write.setLayout(new BorderLayout());
		total_write.add("Center", rightdown_Panel);
		total_write.add("North", left_Panel);
		
		//dig_Write.setLayout(new CardLayout());
		dig_Write.setLayout(new GridLayout(1, 2));
		dig_Write.add("view", total_write);
	}
	
	public void list()
	{
		JPanel buttonPanel = new JPanel();
		
		table.setForeground(Color.black);
		table.getTableHeader().setResizingAllowed(false);
		
		tc1 = new TableColumn(0, 50);
		tc1.setHeaderValue("이       름" );
		tc2 = new TableColumn(1, 100);
		tc2.setHeaderValue("내                     용");
		tc3 = new TableColumn(2, 120);
		tc3.setHeaderValue("주                     소");
		
		dtcm.addColumn(tc1);
		dtcm.addColumn(tc2);
		dtcm.addColumn(tc3);
		buttonPanel.setLayout(new GridLayout(4, 1));
		buttonPanel.add(bt_add);
		buttonPanel.add(bt_modify);
		buttonPanel.add(bt_del);
		buttonPanel.add(excute_bt);
		
		
		//panel_total.setBorder(new TitledBorder("Schedule List"));
		panel_total.setLayout(new BorderLayout(5,5));
		panel_total.add("Center", jsp);
		panel_total.add("East", buttonPanel);
	}
	

	public void action()
	{
		bt_add.addActionListener(this);
		bt_del.addActionListener(this);
		bt_save.addActionListener(this);
		bt_modify.addActionListener(this);
		bt_cancel.addActionListener(this);
		excute_bt.addActionListener(this);
	}
	
	public void actionPerformed(ActionEvent e) //throws IOException
	{
		if(e.getSource() == bt_add)
		{
			dig_Write.setSize(450, 315);
			dig_Write.setLocation(xpos - 150, ypos - 150);
			dig_Write.setResizable(false);
			dig_Write.setVisible(true);	
			//return;
		}
		else if(e.getSource() == bt_del)
		{
			int line = table.getSelectedRow();
			if (line == -1){
				JOptionPane.showMessageDialog(this, "삭제하려는 목록을 선택하세요");
				return;
			}
			
			dtm.removeRow(line); //선택된 Row 삭제
			
			for(int i = 0; i < 3; i++) {
				vData.remove(line*3);
			}
			save();
			return;
		}
		
		else if(e.getSource() == bt_save)
		{
			schdule = tf_Schdule.getText();
			memo = ta_Memo.getText();
			address = url_field.getText();
			info_temp[0] = schdule;
			info_temp[1] = memo;
			info_temp[2] = address;
			
			if(memo.equals("") || address.equals("") || schdule.equals("")) {
				JOptionPane.showMessageDialog(this, "날짜와 일정을 입력해 주세요!", "확인", 
						JOptionPane.INFORMATION_MESSAGE);
				return;
			}
			
			
			if(bt_add.getText() == "수정중"){ // 수정
				for(int i = 0; i < info_temp.length; i++) {
					dtm.setValueAt(info_temp[i], snum, i); // 테이블 채우고	
					vData.remove(snum*3); // 3개 지우고
				}
				
				bt_add.setText("추 가");
				save();
			}else 
				dtm.addRow(info_temp);
			
			// 입력 값을 벡터에 저장
			for (int i = 0; i < info_temp.length; i++ ) {
				vData.addElement(info_temp[i]);
			}
			
			// 필드 초기화
			tf_Schdule.setText("");
			ta_Memo.setText("");
			url_field.setText("");
			
			bt_add.setText("추 가");
			save();
			dig_Write.setVisible(false);
		}
		else if (e.getSource() == bt_modify) {//정보 수정
			
			snum = table.getSelectedRow();
			if (snum == -1){
				JOptionPane.showMessageDialog(this, "수정하려는 목록을 선택하세요");
				return;
			}
			
			for(int i = 0 ; i < info_temp.length; i++) {
				info_temp[i] = (String)dtm.getValueAt(snum, i);
			}
					
			tf_Schdule.setText(info_temp[0]);
			ta_Memo.setText(info_temp[1]);
			url_field.setText(info_temp[2]);
			
			bt_add.setText("수정중");
			dig_Write.setVisible(true);
		}
		
		else if (e.getSource() == bt_cancel)
		{
			dig_Write.setVisible(false);
		}
		else if(e.getSource()==excute_bt){
			int line = table.getSelectedRow();
			if (line == -1){
				JOptionPane.showMessageDialog(this, "삭제하려는 목록을 선택하세요");
				return;
			}
			String output = (String)table.getValueAt(line, 2);
			try{
				Runtime.getRuntime().exec("C:\\Program Files\\Internet Explorer\\IEXPLORE.EXE "+output);
			}
			catch (IOException z) 
			{ 
				z.printStackTrace();	
			} 
		}
	}
	
	public void save() {//벡터의 내용을 파일로 저장
		try {
			File file = new File(URL_PATH, "url");

			BufferedWriter out = new BufferedWriter(new FileWriter(file));
			String[] str2 = new String[vData.size()];
			String str = "";
			for (int i = 0; i < vData.size() ; i++) {
				str2[i] = (String)vData.elementAt(i);
			}
			for (int j = 0; j < str2.length ; j++) {
				if ((j+1)%3 == 0 && (j+1) != str2.length) {
					str += str2[j] + "||\n";
				} else {
					str += str2[j] + "||";
				}
			} 
			out.write(str, 0, str.length());
			//out.newLine();
			out.close();
		}catch (IOException exc) {
			exc.printStackTrace();
		}
	}
	
	public void load() { //파일로 부터 읽어서 벡터로 저장후 테이블에 출력
		File file = new File(URL_PATH, "url");
		try{
			BufferedReader in = new BufferedReader(new FileReader(file));
			String str = "";
			vData = new Vector();
			while ((str = in.readLine()) != null ){
				StringTokenizer token = new StringTokenizer(str, "||" + "\n");
				int i = 0;
				String[] str2 = new String[15]; // while문 안에 있어야 한다.
				while (token.hasMoreTokens()) {
					str2[i] = token.nextToken();
					vData.addElement(str2[i]);
					i++;
				}

				dtm.addRow(str2);
			} 
			in.close();
		}catch( IOException e) {
			e.printStackTrace();
		}
	}

}
