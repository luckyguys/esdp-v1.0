package TaskManager;

import java.awt.GridLayout;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.StringTokenizer;
import java.util.TimeZone;
import java.util.Vector;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Alarm implements Runnable{
	
	
	private static final String FILE_PATH = "resource/file/";  //파일 저장 위치
	private Thread thread;
	private Alarmplay play = new Alarmplay();
	//private Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Asia/Seoul"));
	private Calendar cal;// = Calendar.getInstance(TimeZone.getTimeZone("Asia/Seoul"));
	private SimpleDateFormat form = new SimpleDateFormat();
	private boolean loof =false;
	private long a= 1000L;

	private int temp = 0;
	
	
	private String year, month, date, hours, minute;
	private Vector vData = new Vector(); 
	private String[] info_temp;
	private Scheduler schd = new Scheduler();
	int size = schd.getdtm().getRowCount();
	int[] ar = new int[size];
	//private String[] check;  ///////////////////////

	public Alarm(){
		thread = new Thread(this);
		thread.start();

	}
	@Override
	public void run() {
		
		
		try{
			
		    while(!loof){
		    	 
		    	cal = Calendar.getInstance(TimeZone.getTimeZone("Asia/Seoul"));		
				thread.sleep(a);
				schd.load();
				form.applyPattern("yyyy");
				year = form.format(cal.getTime());
				form.applyPattern("MM");
				month = form.format(cal.getTime());
				form.applyPattern("dd");
				date = form.format(cal.getTime());
				form.applyPattern("HH");
				hours = form.format(cal.getTime());
				form.applyPattern("mm");
				minute = form.format(cal.getTime());
			    String[] day = new String[size];
			    String[] title = new String[size];	
			    String[] check = new String[size];///////////////////////
			    for(int i=0; i<size; i++){
			    	day[i] = (String)schd.gettable().getValueAt(i, 0);
			    	title[i] = (String)schd.gettable().getValueAt(i, 1);
			        check[i] = (String)schd.gettable().getValueAt(i, 2);   ///////////////////
			    	String[] str3 = new String[15];
			   		int t=0;
			   		StringTokenizer token2 = new StringTokenizer(day[i], "." + ":" + "(" + ")");
			   		while(token2.hasMoreTokens()){
			   			str3[t] = token2.nextToken();
			   			t++;
			   		}
		    		if((str3[0].equals(year))&&(str3[1].equals(month))&&(str3[2].equals(date))&&(str3[3].equals(hours))&&(str3[4].equals(minute))&&(check[i].equals("알람"))){
			   			ar[i] = 1;   /////////////////////////////
	
			   		}
			   		else{	
			   			ar[i] = -1;	
			   		}
		    	}
			    
			    for(int y=0; y<size; y++){
			    	if(ar[y]==1){			
			    		play.start();			  
			    		JOptionPane.showMessageDialog(null,  title[y]+ " 해야하는 시간입니다! ", "알 림", 
								JOptionPane.INFORMATION_MESSAGE);
			    		play.stop();
			    		thread.sleep(60000L);
			    		
			    	}
			    }
		   
			}
		}
		catch(Exception e){
				System.out.print(e);
		}
		// TODO Auto-generated method stub		
	}
}


