package TaskManager;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.tree.*;
import javax.swing.table.*;

import java.text.SimpleDateFormat;

public class new_Tree extends JFrame implements TreeWillExpandListener, ActionListener{
	private static final String FILE_PATH = "resource/file/";  //파일 저장 위치


	public Container con_tree;
	private JSplitPane sp=new JSplitPane();
	private DefaultMutableTreeNode root=new DefaultMutableTreeNode("Task list");
	private JTree tree=new JTree(root);
	private JScrollPane tree_jsp=new JScrollPane(tree);
	private String head[]={"Title", "Big", "modify"};
	private DefaultTableModel jdtm = new DefaultTableModel(head, 0){
		boolean[] canEdit = new boolean[]{false , false, false, false, false, false, false};
		public boolean isCellEditable(int rowIndex, int columnIndex){
			return canEdit[columnIndex];
		}
	};
	public DefaultTableColumnModel jdtcm = new DefaultTableColumnModel(); //열너비 조절위해
	public TableColumn tc1, tc2, tc3;
	private JTable table = new JTable(jdtm, jdtcm);
	private JScrollPane table_jsp;
	private JButton del_bt=new JButton("삭  제");
	private JButton read_bt=new JButton("열  기");
	private JPanel jp1;
	private JPanel jp;
	private int xpos, ypos;
	private String[] path=new String[100];
	private int count=0;
	private int count1=0;
	
	public new_Tree(){
		super();
		Dimension screen=Toolkit.getDefaultToolkit().getScreenSize();
		xpos = (int)(screen.getWidth() / 2 - this.getWidth() / 2);
		ypos = (int)(screen.getHeight() / 2 - this.getHeight() / 2);
		load();
		init();
		start();
		save();
		setLocation(0,0);
		setResizable(false);
		
	
	}
	public void init(){
		con_tree = this.getContentPane();
		con_tree.setLayout(new BorderLayout());
		con_tree.add("Center", sp);
		
		tree_jsp.setPreferredSize(new Dimension(200,200));
		
		tc1 = new TableColumn(0, 30);
		tc1.setHeaderValue("파일이름" );
		tc2 = new TableColumn(1, 30);
		tc2.setHeaderValue("최종수정일 ");
		tc3 = new TableColumn(2, 30);
		tc3.setHeaderValue("크             기");
		jdtcm.addColumn(tc1);
		jdtcm.addColumn(tc2);
		jdtcm.addColumn(tc3);
	
		jp1 = new JPanel(new GridLayout(2,1));
		jp1.add(read_bt);
		jp1.add(del_bt);
		table=new JTable(jdtm,jdtcm);
		jp = new JPanel(new BorderLayout());
		jp.add("East", jp1);
		
		sp.setRightComponent(jp);
		sp.setLeftComponent(tree_jsp);
		sp.setResizeWeight(0.3D);
		sp.setDividerLocation(0.15);
		table_jsp=new JScrollPane(table);
		jp.add("Center", table_jsp);
		
		File dir=new File("resource\\filelist\\");  //루트설정
		DefaultMutableTreeNode imsi=new DefaultMutableTreeNode(dir.toString());
		DefaultMutableTreeNode imsi1=new DefaultMutableTreeNode("EMPTY");
		imsi.add(imsi1);
		root.add(imsi);
		
	}
	
	public void start(){
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		tree.addTreeWillExpandListener(this);
		del_bt.addActionListener(this);
		read_bt.addActionListener(this);
	}

public void actionPerformed(ActionEvent e){
		if(e.getSource()==del_bt){
			
			if(table.getSelectedRow()==-1) return;
			TreePath tp=tree.getSelectionPath();
			StringTokenizer stk=new StringTokenizer(tp.toString(), "[,]");
			stk.nextToken();
			if(stk.hasMoreTokens()){
				String filepath=stk.nextToken().trim();
				while(stk.hasMoreTokens()){
					filepath+=stk.nextToken().trim()+"/";
				}
				
				int line = table.getSelectedRow();
				String filename=(String)table.getValueAt(line, 0);
				File f=new File(filepath,filename);
				
				int xx=JOptionPane.showConfirmDialog(this, "삭제하시겠습니까?","삭제",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
				if(xx==0){
					f.delete();
					jdtm.removeRow(line);
					
				}
		
			}
		}
		else if(e.getSource()==read_bt){
			if(table.getSelectedRow()==-1) return;
			
			TreePath tp=tree.getSelectionPath();			
			StringTokenizer stk=new StringTokenizer(tp.toString(), "[,]");
			stk.nextToken();
			if(stk.hasMoreTokens()){
				String filepath=stk.nextToken().trim();
				while(stk.hasMoreTokens()){
					filepath+=stk.nextToken().trim()+"/";
				}
				
				int line = table.getSelectedRow();
				String filename=(String)table.getValueAt(line, 0);
				filepath=makeFilePath(filepath);
			
				if(findExe(filename).equals("java")||findExe(filename).equals("txt")){
					
					File d=new File(path[0]);
					if(d.isFile()){
						try{
							Runtime.getRuntime().exec(path[0]+filepath+filename);
						}
						catch(IOException z){
							z.printStackTrace();
						}
						}
					else{
						String new_path=JOptionPane.showInputDialog(this,"설정이 잘못됬습니다.실행파일 Path입력하세요");
						path[0]=getexePath(new_path)+" ";
						try{
							Runtime.getRuntime().exec(path[0]+filepath+filename);
							}
						catch(IOException z){
							z.printStackTrace();
							}
					save();
					}
					
				}
				else if(findExe(filename).equals("hwp")){
					File d=new File(path[1]);
					if(d.isFile()){
						try{
							Runtime.getRuntime().exec(path[1]+filepath+filename);
							}
						catch(IOException z){
							z.printStackTrace();
							}

					}
					else{
						String new_path=JOptionPane.showInputDialog(this,"path설정이 잘못됬습니다.실행파일의 Path를 입력하세요^^");
						path[1]=getexePath(new_path)+" ";;
						try{
							
							Runtime.getRuntime().exec(path[1]+" "+filepath+filename);
							}
						catch(IOException z){
							z.printStackTrace();
							}
					save();
					}
				}
				else if(findExe(filename).equals("cpp")||findExe(filename).equals("c") || findExe(filename).equals("dsw")){
					File d=new File(path[2]);
					if(d.isFile()){
						try{
							Runtime.getRuntime().exec(path[2]+filepath+filename);
							}
						catch(IOException z){
							z.printStackTrace();
							}

					}	
					else{
						String new_path=JOptionPane.showInputDialog(this,"path설정이 잘못됬습니다.실행파일의 Path를 입력하세요^^");
						path[2]=getexePath(new_path)+" ";;
						try{
							Runtime.getRuntime().exec(path[2]+" "+filepath+filename);
							}
						catch(IOException z){
							z.printStackTrace();
							}
						save();
					}
				}
				
				
				
				
				else if(findExe(filename).equals("mpeg")||findExe(filename).equals("avi")){
					File d=new File(path[3]);
					if(d.isFile()){
						try{
							Runtime.getRuntime().exec(path[3]+filepath+filename);
						}
						catch(IOException z){
						z.printStackTrace();
						}

					}
					else{
						String new_path=JOptionPane.showInputDialog(this,"path설정이 잘못됬습니다.실행파일의 Path를 입력하세요^^");
						path[3]=getexePath(new_path)+" ";;
						try{
							
							Runtime.getRuntime().exec(path[3]+" "+filepath+filename);
							}
						catch(IOException z){
							z.printStackTrace();
							}
					}
				}
				else if(findExe(filename).equals("htm")||findExe(filename).equals("html")){
					File d=new File(path[4]);
					if(d.isFile()){
						try{
							Runtime.getRuntime().exec(path[4]+filepath+filename);
						}
						catch(IOException z){
						z.printStackTrace();
						}
						save();

					}
					else{
						String new_path=JOptionPane.showInputDialog(this,"path설정이 잘못됬습니다.실행파일의 Path를 입력하세요^^");
						path[4]=getexePath(new_path)+" ";;
						try{
							
							Runtime.getRuntime().exec(path[4]+" "+filepath+filename);
							}
						catch(IOException z){
							z.printStackTrace();
							}
					}
				}
				else{
						
						String new_path=JOptionPane.showInputDialog(this,"열고자하는 파일의, 실행파일 Path를 입력하세요^^");
								
						path[count1]=getexePath(new_path)+" ";
						JOptionPane.showMessageDialog(null,path[count1]);
					try{
						Runtime.getRuntime().exec(path[count1]+filepath+filename);
						}
					catch(IOException z){
						z.printStackTrace();
						}
					save();

				}
			}
		}
	}
		
	
	
	public void treeWillExpand(TreeExpansionEvent e){

		if(e.getSource()==tree){
			tree.setSelectionPath(e.getPath());
			String pos=this.getPos(e.getPath());
			if(pos==null|| pos.trim().length()==0) return;
			File f= new File(pos);
			File[] ff=f.listFiles();
			DefaultMutableTreeNode  imsi=(DefaultMutableTreeNode)e.getPath().getLastPathComponent();
			imsi.removeAllChildren();
		
			int size =jdtm.getRowCount();
			for (int i=size-1; i>=0; i--) jdtm.removeRow(i);
			
			
			int cnt=0;
			for(int i=0 ; i<ff.length;++i){
				if(ff[i].isDirectory()){
					DefaultMutableTreeNode imsi1=new DefaultMutableTreeNode(ff[i].getName());
					imsi1.add(new DefaultMutableTreeNode("EMPTY"));
					imsi.add(imsi1);
					cnt++;
				}
			}
			if(cnt==0){
				imsi.add(new DefaultMutableTreeNode("EMPTY"));

			}
			SimpleDateFormat df=new SimpleDateFormat("yyyy년 MM월 dd일");
			
			for(int i=0 ; i<ff.length;++i){
				if(ff[i].isFile()){
					
					Vector vc=new Vector();
					vc.add(ff[i].getName());
					vc.add(df.format(new Date(ff[i].lastModified())));
					vc.add((new Long(ff[i].length())+" bytes"));
					jdtm.addRow(vc);
				}
			}
		
		}
	  
	}
	public void treeWillCollapse(TreeExpansionEvent e){
	}
	
	private String getPos(TreePath tp){
		StringTokenizer tk=new StringTokenizer(tp.toString(),"[,]");
		String str="";
		tk.nextToken();
		while(tk.hasMoreTokens()){
			str+=tk.nextToken().trim()+File.separator;
		}
		return str;

}
	
	public String makeFilePath(String a){			//실행될 패스 설정
		String ret="";
		StringTokenizer stk=new StringTokenizer(a, "/");
		while (stk.hasMoreTokens())  ret+=stk.nextToken()+"\\";
		return ret;

	}
	
	
	public String getexePath(String b){			//실행될 패스 설정
		String ret="";
		StringTokenizer stk=new StringTokenizer(b, "\\");
		ret=stk.nextToken();
		while (stk.hasMoreTokens())  ret+="\\\\"+stk.nextToken();
		return ret;

	}
	
	public String getexebackPath(String c){
		String ret="";
		StringTokenizer stk=new StringTokenizer(c,"\\\\");
		ret=stk.nextToken();
		while(stk.hasMoreTokens()) ret+="\\"+stk.nextToken();
		return ret;
	}
		
	
	public String findExe(String b){			//실행파일확장자 찾아주기 설정
	
		StringTokenizer stk=new StringTokenizer(b,".");
		stk.nextToken();

		return stk.nextToken();


	}
	public void save() {
		try {
			File file = new File(FILE_PATH, "mypath");

			BufferedWriter out = new BufferedWriter(new FileWriter(file));
			
			String str = "";
			
			for (int j = 0; j <count ; j++) {
				str += getexebackPath(path[j]) + "||";
			} 
			out.write(str, 0, str.length());
			out.close();
		}catch (IOException exc) {
			exc.printStackTrace();
			}
	}
	
	
	public void load(){
		try{
			File file = new File(FILE_PATH, "mypath");
			BufferedReader in = new BufferedReader(new FileReader(file));
			String str = "";
			while ((str = in.readLine()) != null ){
				StringTokenizer token = new StringTokenizer(str, "||");
				StringTokenizer token1=new StringTokenizer(str,":");
				count1=token1.countTokens()-1;
				int i = 0;				
				count=token.countTokens();
				while (token.hasMoreTokens()) {
					path[i] = getexePath(token.nextToken());
					i++;
				}
			}
					
			in.close();
			
		}catch( IOException e) {
			e.printStackTrace();
		}
	}
}



