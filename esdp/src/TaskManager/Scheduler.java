package TaskManager;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;
import java.awt.event.*;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

// implements ActionListener, WindowListener
public class Scheduler extends JFrame implements ActionListener{
	public Container con_sched = this.getContentPane();
	
	MainFrame mainF;
	private static final String IMAGE_PATH = "resource/Image/";  //이미지 위치
	private static final String FILE_PATH = "resource/file/";  //파일 저장 위치
	
	private JDialog dig_Write = new JDialog(this, "스케쥴 추가", true);
	
	// 스케쥴 패널

	private JPanel panel_total = new JPanel();
	private JPanel left_Panel = new JPanel();
	private JPanel rightup_Panel = new JPanel();

	
	// 스케쥴 라벨
	private JLabel lb_Year = new JLabel("년");
	private JLabel lb_tab = new JLabel("          ");
	private JTextField tf_year = new JTextField(3);
	private JLabel lb_Month = new JLabel(" 월");
	private JTextField tf_Month = new JTextField(3);
	private JLabel lb_Day = new JLabel(" 일  ");
	private JTextField tf_Day = new JTextField(3);
	private JLabel lb_Hour = new JLabel(" 시" );
	private JTextField tf_Hour = new JTextField(3);
	private JLabel lb_Min = new JLabel(" 분 ");
    private JTextField tf_Min = new JTextField(3);
	
    private JTextField tf_Schdule = new JTextField(37);
	
	private JTextArea ta_Memo = new JTextArea();
	
	private JScrollPane sp_Memo = new JScrollPane(ta_Memo);
	
	private JButton bt_today = new JButton("Today");
	private JButton bt_save = new JButton("등            록");
	private JButton bt_cancel = new JButton("취            소");
	private JButton bt_search = new JButton("검 색");
	private JButton bt_add = new JButton("추 가");
	private JButton bt_modify = new JButton("수 정");
	private JButton bt_del = new JButton("삭 제");
	
	
	// 날짜 얻어오기
	private Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Asia/Seoul"));
	private SimpleDateFormat form = new SimpleDateFormat();
	private String year, month, day;
	
	// 스케쥴 추가 수정
	private String[] info_temp = new String[3];
	private String daytime, schdule, memo;
	private int snum;
	
	//리스트출력
	private String[] str = {"일 시", "일 정", "메 모"};  
	private DefaultTableModel dtm = new DefaultTableModel(str, 0){
		boolean[] canEdit = new boolean[]{false , false, false, false, false, false, false};
		public boolean isCellEditable(int rowIndex, int columnIndex){
			return canEdit[columnIndex];
		}
	};
	private DefaultTableColumnModel dtcm = new DefaultTableColumnModel(); //열너비 조절위해
	private DefaultTableCellRenderer dtcr = new DefaultTableCellRenderer();
	private JTable table = new JTable(dtm, dtcm);
	private JScrollPane jsp = new JScrollPane(table);
	private TableColumn tc1, tc2, tc3; //열 너비 지정

	private Dimension screen;
	private int xpos, ypos;
	
	private Vector vData = new Vector(); 
	public Scheduler(MainFrame mainframe)
	{
		mainF = mainframe;
		init();
		left();
		right();
		list();
		write();
		action();
		load();
		screen = Toolkit.getDefaultToolkit().getScreenSize();
		xpos = (int)(screen.getWidth() / 2 - this.getWidth() / 2);
		ypos = (int)(screen.getHeight() / 2 - this.getHeight() / 2);
	}
	public Scheduler(){
		init();
		left();
		right();
		list();
		write();
		action();
		load();
		screen = Toolkit.getDefaultToolkit().getScreenSize();
		xpos = (int)(screen.getWidth() / 2 - this.getWidth() / 2);
		ypos = (int)(screen.getHeight() / 2 - this.getHeight() / 2);
	}
	public void init(){
		con_sched.add(panel_total);
		
	}

	public void left()
	{
		left_Panel.setBorder(new TitledBorder("일시 및 제목"));
		left_Panel.setLayout(new BorderLayout());
		
		JPanel left_date = new JPanel();
		JPanel subPanel = new JPanel();
		JPanel left_title = new JPanel();
		
		//날짜 불러오기
		form.applyPattern("yyyy");
		year = form.format(cal.getTime());
		form.applyPattern("MM");
		month = form.format(cal.getTime());
		form.applyPattern("dd");
		day = form.format(cal.getTime());
		
		today();
		
		//나열하기
		subPanel.setLayout(new BorderLayout());
		
		left_date.setLayout(new FlowLayout());
		left_date.add(tf_year);
		left_date.add(lb_Year);
		left_date.add(tf_Month);
		left_date.add(lb_Month);
		left_date.add(tf_Day);
		left_date.add(lb_Day);
		
		left_date.add(bt_today);
		bt_today.setPreferredSize(new Dimension(50, 30));
		bt_today.setMargin(new Insets(0, 0, 0, 0));
		
		left_date.add(lb_tab);
		left_date.add(tf_Hour);
		left_date.add(lb_Hour);
		left_date.add(tf_Min);
		left_date.add(lb_Min);
		
		left_title.setBorder(new TitledBorder("제 목"));
		left_title.add(tf_Schdule);
		
		subPanel.add("North", left_date);
		subPanel.add("South", left_title);
		
		left_Panel.setLayout(new GridLayout(1, 3));
		left_Panel.add(subPanel);
	}
	public void right()
	{
		rightup_Panel.setBorder(new TitledBorder("내용 입력"));
		rightup_Panel.setLayout(new BorderLayout());
		
		JPanel right_add = new JPanel();
		
		ta_Memo.setLineWrap(true);
		sp_Memo.setWheelScrollingEnabled(true);
		sp_Memo.setAutoscrolls(true);
		sp_Memo.setPreferredSize(new Dimension(410, 72));
		right_add.add("Center", sp_Memo);
		
		bt_save.setPreferredSize(new Dimension(200, 23 ));
		bt_cancel.setPreferredSize(new Dimension(200, 23 ));
		
		rightup_Panel.setLayout(new FlowLayout());
		rightup_Panel.add(right_add);
		rightup_Panel.add(bt_save);
		rightup_Panel.add(bt_cancel);
	}
	
	public void write()
	{
		JPanel total_write = new JPanel();
		total_write.setLayout(new BorderLayout());
		total_write.add("Center", rightup_Panel);
		total_write.add("North", left_Panel);
		
		dig_Write.setLayout(new GridLayout(1, 2));
		dig_Write.add("view", total_write);
		dig_Write.addWindowListener(new WindowAdapter() {
			   public void windowClosing(WindowEvent e) {
				   bt_add.setText("추 가");
				   
				   tf_Hour.setText("00");
				   tf_Min.setText("00");
				   tf_Schdule.setText("");
				   ta_Memo.setText("");
				   }
				  });
	}
	
	public void list()
	{
		JPanel buttonPanel = new JPanel();
		
		table.setForeground(Color.black);
		table.getTableHeader().setResizingAllowed(false);
		
		tc1 = new TableColumn(0, 50);
		tc1.setHeaderValue("일 시" );
		tc2 = new TableColumn(1, 150);
		tc2.setHeaderValue(" 스      케      쥴  ");
		tc3 = new TableColumn(2, 70);
		tc3.setHeaderValue(" 비          고 ");
		
		dtcm.addColumn(tc1);
		dtcm.addColumn(tc2);
		dtcm.addColumn(tc3);
		buttonPanel.setLayout(new GridLayout(3, 1));
		buttonPanel.add(bt_add);
		buttonPanel.add(bt_modify);
		buttonPanel.add(bt_del);
		
		
		panel_total.setLayout(new BorderLayout());
		panel_total.add("Center", jsp);
		panel_total.add("East", buttonPanel);
	}
	
	public void today()
	{
		tf_year.setText(year);
		tf_Month.setText(month);
		tf_Day.setText(day);
		tf_Hour.setText("00");
		tf_Min.setText("00");
	}
	
	public void action()
	{
		bt_add.addActionListener(this);
		bt_del.addActionListener(this);
		bt_save.addActionListener(this);
		bt_modify.addActionListener(this);
		bt_today.addActionListener(this);
		bt_cancel.addActionListener(this);
	}
	
	public void actionPerformed(ActionEvent e)
	{
		if(e.getSource() == bt_add)
		{
			dig_Write.setSize(450, 300);
			dig_Write.setLocation(xpos - 150, ypos - 150);
			dig_Write.setResizable(false);
			dig_Write.setVisible(true);	
			
			return;
		}
		else if(e.getSource() == bt_del)
		{
			int line = table.getSelectedRow();
			if (line == -1){
				JOptionPane.showMessageDialog(this, "삭제하려는 목록을 선택하세요");
				return;
			}
			
			dtm.removeRow(line); //선택된 Row 삭제
			
			for(int i = 0; i < 3; i++) {
				vData.remove(line*3);
			}
			save();
			return;
		}
		
		else if(e.getSource() == bt_save)
		{
			if((tf_Month.getText().length() < 2)) {
				tf_Month.setText("0" + tf_Month.getText());
			}
			if(tf_Day.getText().length() < 2)	{
				tf_Day.setText("0" + tf_Day.getText());
			}
			if(tf_Hour.getText().length() < 2)	{
				tf_Hour.setText("0" + tf_Hour.getText());
			}
			if(tf_Min.getText().length() < 2)	{
				tf_Min.setText("0" + tf_Min.getText());
			}
			
			
			daytime = tf_year.getText() +"."+ tf_Month.getText() + "." + tf_Day.getText() +
			          "(" + tf_Hour.getText() + ":"+  tf_Min.getText() + ")";
			schdule = tf_Schdule.getText();
			memo = ta_Memo.getText();
			info_temp[0] = daytime;
			info_temp[1] = schdule;
			info_temp[2] = memo;
			if(tf_year.getText().equals("") || tf_Month.getText().equals("") || tf_Day.getText().equals("") || tf_Hour.getText().equals("") || tf_Min.getText().equals("") || schdule.equals("") || memo.equals("")) {
				JOptionPane.showMessageDialog(this, "날짜와 일정을 입력해 주세요!", "확인", 
						JOptionPane.INFORMATION_MESSAGE);
				return;
			}
			if(Integer.parseInt(tf_year.getText()) > 2100 || Integer.parseInt(tf_Month.getText()) > 12 || Integer.parseInt(tf_Day.getText()) > 31 || Integer.parseInt(tf_Hour.getText()) > 25 || Integer.parseInt(tf_Min.getText()) > 60) {
				JOptionPane.showMessageDialog(this, "범위를 초과된 값을 입력하셨습니다.!", "확인", 
						JOptionPane.INFORMATION_MESSAGE);
				return;
			}
			
			if(bt_add.getText() == "수정중"){ // 수정
				for(int i = 0; i < info_temp.length; i++) {
					dtm.setValueAt(info_temp[i], snum, i); // 테이블 채우고	
					vData.remove(snum*3); // 3개 지우고
				}
				
				bt_add.setText("추 가");
				save();
			}else 
				dtm.addRow(info_temp);
			
			// 입력 값을 벡터에 저장
			for (int i = 0; i < info_temp.length; i++ ) {
				vData.addElement(info_temp[i]);
			}
			
			// 필드 초기화
			tf_Hour.setText("");
			tf_Min.setText("");
			tf_Schdule.setText("");
			ta_Memo.setText("");
			
			bt_add.setText("추 가");
			save();
			dig_Write.setVisible(false);
		}
		else if (e.getSource() == bt_modify) {//정보 수정
			
			snum = table.getSelectedRow();
			if (snum == -1){
				JOptionPane.showMessageDialog(this, "수정하려는 목록을 선택하세요");
				return;
			}
			
			for(int i = 0 ; i < info_temp.length; i++) {
				info_temp[i] = (String)dtm.getValueAt(snum, i);
			}
			StringTokenizer token = new StringTokenizer(info_temp[0], "." + ":" + "(" + ")");
			String[] str = new String[token.countTokens()]; // while문 안에 있어야 한다.
			int i = 0;
			while (token.hasMoreTokens()) {
				str[i] = token.nextToken();
				i++;
			}
			tf_year.setText(str[0]);
			tf_Month.setText(str[1]);
			tf_Day.setText(str[2]);
			tf_Hour.setText(str[3]);
			tf_Min.setText(str[4]);			
			tf_Schdule.setText(info_temp[1]);
			ta_Memo.setText(info_temp[2]);
			
			bt_add.setText("수정중");
			dig_Write.setSize(450, 300);
			dig_Write.setLocation(xpos - 150, ypos - 150);
			dig_Write.setResizable(false);
			dig_Write.setVisible(true);
		}
		else if (e.getSource() == bt_today)
		{
			today();
		}
		else if (e.getSource() == bt_cancel)
		{
			bt_add.setText("추 가");
			dig_Write.setVisible(false);
		}
	}
	
	public void save() {//벡터의 내용을 파일로 저장
		try {
			File file = new File(FILE_PATH, "Schedule");

			BufferedWriter out = new BufferedWriter(new FileWriter(file));
			String[] str2 = new String[vData.size()];
			String str = "";
			for (int i = 0; i < vData.size() ; i++) {
				str2[i] = (String)vData.elementAt(i);
			}
			for (int j = 0; j < str2.length ; j++) {
				if ((j+1)%3 == 0 && (j+1) != str2.length) {
					str += str2[j] + "||\n";
				} else {
					str += str2[j] + "||";
				}
			} 
			out.write(str, 0, str.length());
			out.close();
			MainFrame m = new MainFrame(true);
		}catch (IOException exc) {
			exc.printStackTrace();
		}
	}
	
	public void load() { //파일로 부터 읽어서 벡터로 저장후 테이블에 출력
		File file = new File(FILE_PATH, "Schedule");
		try{
			BufferedReader in = new BufferedReader(new FileReader(file));
			String str = "";
			vData = new Vector();
			while ((str = in.readLine()) != null ){
				StringTokenizer token = new StringTokenizer(str, "||" + "\n");
				int i = 0;
				String[] str2 = new String[15]; // while문 안에 있어야 한다.
				while (token.hasMoreTokens()) {
					str2[i] = token.nextToken();
					vData.addElement(str2[i]);
					i++;
				}

				dtm.addRow(str2);
			} 
			in.close();
			
			dtcr.setHorizontalAlignment(SwingConstants.CENTER);
			TableColumnModel tcm = table.getColumnModel();
			for(int j = 0; j < dtm.getColumnCount(); j++)
			{
				tcm.getColumn(j).setCellRenderer(dtcr);
			}
		}catch( IOException e) {
			e.printStackTrace();
		}
	}
	public String getList(String date)
	{
		String retValue = "";
		String temp = "";
		int col = 0;
		int row = -1;
		for(int i = 0; i < dtm.getRowCount(); i++)
		{
			temp = (String)table.getValueAt(i, col);
			if(temp.indexOf(date) != -1)
			{
				row = i;
				break;
			}
		}
		if(row == -1)
			return "없다";
		for(int i = 0 ; i < info_temp.length; i++) {
			info_temp[i] = (String)dtm.getValueAt(row, i);
		}
		
		retValue = info_temp[0] + "||" + info_temp[1] + "||" + info_temp[2];
		return retValue;
	}
	
	public JTable gettable(){
		return table;
	}
	public DefaultTableModel getdtm(){
		return dtm;
	}
}
