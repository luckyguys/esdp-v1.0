package TaskManager;

import java.io.File;

import javax.sound.midi.MidiSystem;
import javax.sound.midi.Sequence;
import javax.sound.midi.Sequencer;

public class Alarmplay {
	private static final String ALARM_PATH = "resource/alarm/";  //파일 저장 위치
	private Sequencer sequencer = null; //음악 파일이고

	private Sequence sequence = null; //플레이어라고 생각하면 쉽다.

	Alarmplay() {
		try {

			//파일 객체로부터 시퀀스를 얻어 온다.

			//아래 스트링과 같은 이름의 파일이 존재해야 한다.
			sequence=MidiSystem.getSequence(new File(ALARM_PATH + "alarm.mid"));

			sequencer=MidiSystem.getSequencer(); //기본 시퀀서를 얻어 온다.

			sequencer.open(); // 시퀀서를 연다.
			sequencer.setSequence(sequence); //시퀀서에 시퀀스를 설정한다.
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	Alarmplay(String f) {
		try {

			//파일 객체로부터 시퀀스를 얻어 온다.

			//아래 스트링과 같은 이름의 파일이 존재해야 한다.
			sequence=MidiSystem.getSequence(new File(ALARM_PATH + f));

			sequencer=MidiSystem.getSequencer(); //기본 시퀀서를 얻어 온다.

			sequencer.open(); // 시퀀서를 연다.
			sequencer.setSequence(sequence); //시퀀서에 시퀀스를 설정한다.
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	void start() {
		try {
			sequencer.stop(); //이미 곡을 연주하고 있을 때 새로 듣기 위해 먼저 멈춘다.
			sequencer.start(); //곡을 연주한다.
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	void stop(){
		try {
			sequencer.stop(); //이미 곡을 연주하고 있을 때 새로 듣기 위해 먼저 멈춘다.
			//sequencer.start(); //곡을 연주한다.
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	

}
