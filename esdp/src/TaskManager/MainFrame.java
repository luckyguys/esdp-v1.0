package TaskManager;



import java.awt.event.*;
import java.awt.*;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.border.TitledBorder;
import javax.swing.plaf.metal.*;
import javax.swing.*;

public class MainFrame extends JFrame implements ActionListener, WindowFocusListener{
	private Container contentPane = super.getContentPane();
	private static final String IMAGE_PATH = "resource/Image/";
	private static final String ALARM_PATH = "resource/alarm/";
	
	private Scheduler  scheduler;
	private Assignments  assignments;
	private Diary  diary;
	private Address  address;
	private Calendars cal;
	private new_Tree tree;
	private Alarmplay sound;
	private D_clock timer;
	private Url url;  
	
	private JPanel Home = new JPanel();
	private JPanel mainPanel;
	private JPanel mainPanel2;
	private JPanel menuPanel;
	private JPanel calendarPanel;
	private JPanel taskListPanel;
	private JPanel fileListPanel;
	private JPanel buttonPanel;
	
	private JMenuBar menuBar;
	private JMenu userMenu, editMenu, helpMenu;
	private JMenuItem protect, unprotect, first, exit;
	private JMenuItem add, modify;
	private JMenuItem option, inforHelp;
	
	
	private JToolBar toolBar;
	private JButton button1;
	private JButton button2;
	private JButton button3;
	private JButton button4;
	private JButton button5;
	private JButton button6; 
	private JButton search;
	private JButton button7; //노래 off
	
	private JTabbedPane jtp = new JTabbedPane(JTabbedPane.TOP, JTabbedPane.SCROLL_TAB_LAYOUT);
	
	private Font font;
	private Color color;
	//////////////////////프로텍트 부분
	private static final String PASSWORD_PATH = "resource/file/";  //파일 저장 위치
	private JDialog password = new JDialog(this, "지금은 자리비움 중입니다!!", true);
	// Container con_pro = this.getContentPane();
	private JPanel total_panel = new JPanel();
	private JLabel pass_label = new JLabel("PASSWORD 입력");
	
	private JButton bt1 = new JButton("설   정");
	private JButton bt2 = new JButton("입   력");
	
	private JTextField text = new JTextField(3);
	
	Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
	private int xpos = (int)(screen.getWidth() / 2 - this.getWidth() / 2);
	private int ypos = (int)(screen.getHeight() / 2 - this.getHeight() / 2);
	
	
	
	
	
	
	///////////////////////////////	
	
	public MainFrame(){
		
		
		super("ESDP v1.0 (By LUCKYGUYS)");
	
		doalarm();
		scheduler = new Scheduler(this);
		diary = new Diary();
		assignments = new Assignments();
		address = new Address();
		url = new Url(); 
			
		cal = new Calendars(this);
	   
		font = new Font("개성9", 0, 15);
    	color = new Color(153, 204, 255);
    	
		menu();
		setHome();
		calendar();
		tasklist();
		button();
		Filelist();
		init();
		init_protect();
		action();  
		this.setSize(600, 700);
		
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation((int)(screen.getWidth()/2  -  this.getWidth() / 2),(int)(screen.getHeight()/2  -  this.getHeight() / 2));
		this.setResizable(false);
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		cal.start_Task();
	}
	
	public MainFrame(boolean p){
		doalarm();
	}
	
	public void doalarm(){
		Alarm alarm = new Alarm();
		
	}
	public void setHome()
	{
		JLabel label_img = new JLabel(new ImageIcon(IMAGE_PATH+"home.jpg"));
		Home.setLayout(new GridLayout(1, 1));
		Home.add(label_img);
	}
	public void init(){
		mainPanel = new JPanel();
		mainPanel2 = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		mainPanel2.setLayout(new BorderLayout());
		Dimension calendar_Panelsize = new Dimension(600, 190);
		Dimension taskList_Panelsize = new Dimension(600, 600);
		Dimension fileList_Panelsize = new Dimension(600, 120);
		
		calendarPanel.setPreferredSize(calendar_Panelsize);
		taskListPanel.setPreferredSize(taskList_Panelsize);
		fileListPanel.setPreferredSize(fileList_Panelsize);
		
		mainPanel.add(calendarPanel, BorderLayout.NORTH);
		mainPanel.add(taskListPanel, BorderLayout.CENTER);
		mainPanel.add(fileListPanel, BorderLayout.SOUTH);
	
		mainPanel2.setBorder(new TitledBorder(""));
		mainPanel2.add(mainPanel);
		contentPane.add(menuPanel, "North");
		
		contentPane.add(mainPanel2, "Center");
		contentPane.add(buttonPanel, "South");
	}
	public void setUI(){  
		try {
			
				SwingUtilities.updateComponentTreeUI(this);
			  }catch(Exception e) {};
	}
	public void menu(){
		/* menuBar;
		 userMenu, editMenu, helpMenu;
		 user, load, exit;
		 add, modify;
		option, inforHelp;
		*/
		menuPanel = new JPanel();
		menuBar = new JMenuBar();
		userMenu = new JMenu("시스템(S)");
		userMenu.setMnemonic('S');
		userMenu.setFont(font);
		helpMenu = new JMenu("도움말(H)");
		helpMenu.setMnemonic('H');
		helpMenu.setFont(font);
		
		protect = new JMenuItem("보호모드(P)");
		protect.setMnemonic('P');
		protect.setFont(font);
		protect.addActionListener(this);
		exit = new JMenuItem("종  료(Q)");
		exit.setMnemonic('Q');
		exit.setFont(font);
		exit.addActionListener(this);
		
		userMenu.add(protect);
		userMenu.add(exit);
		
		/************/
		
		option = new JMenuItem("등록정보");
		inforHelp = new JMenuItem("ESDP 가이드");
		
		helpMenu.add(option);
		helpMenu.add(inforHelp);
		menuBar.setBackground(color);
		menuBar.add(userMenu);
		menuBar.add(helpMenu);
		menuPanel.setLayout(new GridLayout());
		menuPanel.add(menuBar);
		
		option.addActionListener(this);
		inforHelp.addActionListener(this);
	}
	
	public void calendar(){
		calendarPanel = new JPanel();
		calendarPanel.setLayout(new BorderLayout());
		calendarPanel.add(cal, "Center");
	}
	
	public Scheduler getSche()
	{
		return scheduler;
	}
	public Diary getDiary()
	{
		return diary;
	}
	public Assignments getAssign()
	{
		return assignments;
	}
	public void tasklist(){
		taskListPanel= new JPanel();
		taskListPanel.setLayout(new BorderLayout());
		jtp.addTab("메   인",new ImageIcon(IMAGE_PATH+"home.png"), Home);
		jtp.addTab("일정관리",new ImageIcon(IMAGE_PATH+"schedule.png"), scheduler.con_sched);
		jtp.addTab("업무관리",new ImageIcon(IMAGE_PATH+"assign.gif"), assignments.con_assign);
		jtp.addTab("다이어리",new ImageIcon(IMAGE_PATH+"diary.png"), diary.con_Diary);
		jtp.addTab("주 소 록",new ImageIcon(IMAGE_PATH+"address.png"), address.con_Addr);
		jtp.addTab("즐겨찾기",new ImageIcon(IMAGE_PATH+"url.gif"), url.con_Url); // 즐겨찾기 메뉴에 추가 - 상열
		jtp.setFont(new Font("휴먼편지체", Font.BOLD, 15));
		taskListPanel.add(jtp,new BorderLayout().CENTER);
		
	}
	public void Filelist(){
		fileListPanel = new JPanel();
		tree = new new_Tree();
		fileListPanel.setLayout(new BorderLayout());
		fileListPanel.add(tree.con_tree,"Center");
	}
	
	public void button(){
		button1 = new JButton(new ImageIcon(IMAGE_PATH+"HWP.JPG"));
		Dimension buttonSize = new Dimension(50, 20);
		button1.setPreferredSize(buttonSize);
		button2 = new JButton(new ImageIcon(IMAGE_PATH+"ECLIPSE.JPG"));
		button2.setPreferredSize(buttonSize);
		button3 = new JButton(new ImageIcon(IMAGE_PATH+"C++.JPG"));
		button3.setPreferredSize(buttonSize);
	
		button5 = new JButton(new ImageIcon(IMAGE_PATH+"explorer.jpg"));
		button5.setPreferredSize(buttonSize);
	    
		button6 = new JButton(new ImageIcon(IMAGE_PATH+"hanyang.jpg"));
		button6.setPreferredSize(buttonSize);
		
		search = new JButton(new ImageIcon(IMAGE_PATH+"search.jpg"));
		search.setPreferredSize(buttonSize);
		
		button7 = new JButton("off");
		button7.setPreferredSize(buttonSize);
		
		
		buttonPanel = new JPanel();
		toolBar = new JToolBar();
		
		toolBar.add(button1);
		toolBar.add(button2);
		toolBar.add(button3);
		
		//toolBar.add(button4);
		toolBar.add(button5);
		
		toolBar.add(button6);
		toolBar.add(search);
		toolBar.addSeparator();
		toolBar.addSeparator();
		toolBar.add(button7);
	
		toolBar.setBackground(color);
		buttonPanel.setBackground(color);
		buttonPanel.setLayout(new BorderLayout());
		buttonPanel.setPreferredSize(new Dimension(400, 50));
		buttonPanel.add("Center",toolBar);
		
		
		timer = new D_clock();
		buttonPanel.add("East", timer);
		
	}
	
	////////////////////////프로텍트 메소드
	public void init_protect(){
		total_panel.setLayout(new BorderLayout());
		total_panel.add("North", pass_label);
		total_panel.add("Center", text);
		JPanel button_panel = new JPanel();
		button_panel.setLayout(new GridLayout(1,2,5,5));
		button_panel.add(bt1);
		button_panel.add(bt2);
		total_panel.add("South", button_panel);
		
		
		password.add(total_panel);
		
		
	}
	
	
	
	
	////////////////////////
	
	
	public void action(){
		bt1.addActionListener(this);
		bt2.addActionListener(this);
		button1.addActionListener(this);
		button2.addActionListener(this);
		button3.addActionListener(this);
		//button4.addActionListener(this);
		button5.addActionListener(this);
		button6.addActionListener(this);
		search.addActionListener(this);
	    button7.addActionListener(this);
		sound = new Alarmplay("dream.mid");
		sound.start();
	}

	@Override
	public void windowGainedFocus(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowLostFocus(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}
	public static void main(String [] args){
		MainFrame mainFrame = new MainFrame();
		
	
	}
	
	public void openWebPage(String url){
		   try {         
		     java.awt.Desktop.getDesktop().browse(java.net.URI.create(url));
		   }
		   catch (java.io.IOException e) {
		       System.out.println(e.getMessage());
		   }
		}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
		if(e.getSource() == exit){
			System.exit(1);
		}
		
		if(e.getSource() == option){
			String option_d="                       ESDP v1.0  \n"+" 2011004352 김동혁  2011004750 최창렬\n  " +"              2011004471 박정헌";
			JOptionPane.showMessageDialog(this, option_d, "ESDP Develop Infor", 
					JOptionPane.INFORMATION_MESSAGE);
		}
		if(e.getSource() == inforHelp){
			String option_d="문의사항은 \n tyranny190@hanyang.ac.kr (박정헌)";
			JOptionPane.showMessageDialog(this, option_d, "ESDP develop manager", 
					JOptionPane.INFORMATION_MESSAGE);
		}
		else if(e.getSource() ==button7){
			
			sound.stop();
		}
		else if(e.getSource()==protect){
			password.setDefaultCloseOperation(0);
			bt1.setEnabled(true);
			text.setText("");
			password.setSize(230, 100);
			password.setLocation(xpos-115, ypos-75);
			password.setResizable(false);
			password.setVisible(true);
			//password.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		}
		else if(e.getSource()==bt1){
			try{
				File file = new File(PASSWORD_PATH, "password");
				password.setDefaultCloseOperation(0);
				BufferedWriter out = new BufferedWriter(new FileWriter(file));
				if( text.getText().equals(""))return;

				out.write(text.getText());
				out.close();
			
				text.setText("");
				bt1.setEnabled(false);
			}
			catch (IOException exc) {
				exc.printStackTrace();
			}
		}
		else if(e.getSource()==bt2){
			try{
				File file = new File(PASSWORD_PATH, "password");
				setDefaultCloseOperation(0);
				BufferedReader in = new BufferedReader(new FileReader(file));
				if( text.getText().equals(""))return;
			
				String pass = in.readLine();
				if(pass.equals(text.getText())){
					JOptionPane.showMessageDialog(null, "PASSWORD 일치");
					
					password.setVisible(false);
					this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					
				}
				else{
					text.setText("");
					JOptionPane.showMessageDialog(null, "PASSWORD 불일치");
				}
				in.close();
				
			}
			catch (IOException exc) {
				exc.printStackTrace();
			}	
		}
		else if(e.getSource() ==button1){   /////////////1~3, 5,
			try{
				Runtime.getRuntime().exec("C:\\HNC\\Hwp65\\Hwp.exe");  //컴터마다 주소 변경
			}
			catch (IOException z) 
			{ 
				z.printStackTrace();	
			}
		}
		else if(e.getSource() ==button2){
			/*try{
				Runtime.getRuntime().exec("C:\\HNC\\Hwp65\\Hwp.exe");  //컴터마다 주소 변경
			}
			catch (IOException z) 
			{ 
				z.printStackTrace();	
			} */
		}
		else if(e.getSource() ==button3){
			try{
				Runtime.getRuntime().exec("C:\\Program Files\\Microsoft Visual Studio\\Common\\MSDev98\\Bin\\MSDEV.EXE");  //컴터마다 주소 변경
			}
			catch (IOException z) 
			{ 
				z.printStackTrace();	
			} 
		}
		else if(e.getSource() ==button5){
			try{
				Runtime.getRuntime().exec("C:\\Program Files\\Internet Explorer\\IEXPLORE.EXE");  //컴터마다 주소 변경
			}
			catch (IOException z) 
			{ 
				z.printStackTrace();	
			} 
		}
		else if(e.getSource() ==button6){
			try {         
				openWebPage("http://www.hanyang.ac.kr");
			   }
			   catch (Exception z) {
			       z.printStackTrace();
			   }
		}
		else if(e.getSource() ==search){
			try {     
				 String myword = JOptionPane.showInputDialog(null,
					      "Search: ",
					      "Naver search", JOptionPane.QUESTION_MESSAGE);

				openWebPage("http://search.naver.com/search.naver?sm=tab_hty.top&where=nexearch&ie=utf8&query="+myword);
			   }
			   catch (Exception z) {
			       z.printStackTrace();
			   }
		}
	}
	
}