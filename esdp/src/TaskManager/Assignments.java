package TaskManager;

import java.awt.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;

import java.awt.event.*;
import java.io.*;
import java.awt.Color.*;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.StringTokenizer;
import java.util.TimeZone;
import java.util.Vector;

public class Assignments extends JFrame implements ActionListener/*, MouseListener*/{
	private static final String IMAGE_PATH = "resource/Image/";  //이미지 위치
	private static final String FILE_PATH = "resource/file/";  //파일 저장 위치
	public Container con_assign = this.getContentPane();	
	
	private String head[]={"Title", "Date1", "Date2", "Text", "State"};
	private DefaultTableModel dtm = new DefaultTableModel(head, 0){
		boolean[] canEdit = new boolean[]{false , false, false, false, false, false, false};
		public boolean isCellEditable(int rowIndex, int columnIndex){
			return canEdit[columnIndex];
		}
	};
	

	public DefaultTableColumnModel dtcm = new DefaultTableColumnModel(); //열너비 조절위해
	private DefaultTableCellRenderer dtcr = new DefaultTableCellRenderer();
	
	public TableColumn tc1, tc2, tc3, tc4, tc5; //열 너비 지정
	public JTable j_table = new JTable(dtm,dtcm);
	private JPanel button_panel = new JPanel();
	
	private JButton bt_add = new JButton("추 가");
	private JButton button2 = new JButton("수 정");
	private JButton button3 = new JButton("삭 제");
	
	private Dimension screen;
	private int xpos, ypos;
	//새로 만드는 다이얼로그
	private JDialog dig_Write = new JDialog(this, "과제", true);
	// 스케쥴 패널

	private JPanel panel_total = new JPanel();
	private JPanel left_Panel = new JPanel();
	private JPanel rightup_Panel = new JPanel();

	
	// 스케쥴 라벨
	//시작날짜
	private JLabel lb_Year = new JLabel("년");
	private JLabel lb_tab = new JLabel("          ");
	private JTextField tf_year = new JTextField(3);
	private JLabel lb_Month = new JLabel(" 월");
	private JTextField tf_Month = new JTextField(3);
	private JLabel lb_Day = new JLabel(" 일  ");
	private JTextField tf_Day = new JTextField(3);
	private JLabel lb_Hour = new JLabel(" 시" );
	private JTextField tf_Hour = new JTextField(3);
	private JLabel lb_Min = new JLabel(" 분 ");
    private JTextField tf_Min = new JTextField(3);
	//제출날짜
    private JLabel lb_Year2 = new JLabel("년");
	private JLabel lb_tab2 = new JLabel("                            ");
	
	private JTextField tf_year2 = new JTextField(3);
	private JLabel lb_Month2 = new JLabel(" 월");
	private JTextField tf_Month2 = new JTextField(3);
	private JLabel lb_Day2 = new JLabel(" 일  ");
	private JTextField tf_Day2 = new JTextField(3);
	private JLabel lb_Hour2 = new JLabel(" 시" );
	private JTextField tf_Hour2 = new JTextField(3);
	private JLabel lb_Min2 = new JLabel(" 분 ");
    private JTextField tf_Min2 = new JTextField(3);
    //
    
    private JTextField tf_Schdule = new JTextField(37);
	
	private JTextArea ta_Memo = new JTextArea("");
	
	private JScrollPane sp_Memo = new JScrollPane(ta_Memo);
	
	
	private JTextField link_field =  new JTextField(25); //링크  필드 
	
	private JButton bt_today = new JButton("Today");
	private JButton bt_save = new JButton("등              록");
	private JButton bt_cancel = new JButton("취              소");
	private JButton link_button = new JButton("완     료");


	
	// 날짜 얻어오기
	private Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Asia/Seoul"));
	private SimpleDateFormat form = new SimpleDateFormat();
	private String year, month, day;
	
	
	private Vector vData; 
	private String[] m_values = new String[5];
	private String title, daytime1, daytime2, text, link;
	private int snum;
	private int num;  //갱신할려고 사용
	
	public Assignments(){
		
		init();
		east();;
		left();
		right();
		write();
		file_load();
		action();
		
		screen = Toolkit.getDefaultToolkit().getScreenSize();
		xpos = (int)(screen.getWidth() / 2 - this.getWidth() / 2);
		ypos = (int)(screen.getHeight() / 2 - this.getHeight() / 2);
	}
	
	public void action(){
		bt_add.addActionListener(this);  //추가
		button2.addActionListener(this);  //보기
		button3.addActionListener(this);  //삭제
		bt_save.addActionListener(this); //다이얼로그 등록
		bt_cancel.addActionListener(this); //다이얼로그 취소		
		bt_today.addActionListener(this);
		link_button.addActionListener(this);
		
	}
	
	public void init(){
		
		Dimension buttonSize = new Dimension(50, 50);
		bt_add.setPreferredSize(buttonSize);
		int a=20;
		tc1 = new TableColumn(0, 70);
		tc1.setHeaderValue("과  제  명" );
		tc2 = new TableColumn(1, 80);
		tc2.setHeaderValue(" 시   작   일  ");
		tc3 = new TableColumn(2, 80);
		tc3.setHeaderValue(" 제   출   일  ");
		tc4 = new TableColumn(3, 100);
		tc4.setHeaderValue(" 내               용  ");
		tc5 = new TableColumn(4, 30);
		tc5.setHeaderValue(" 상        태 ");
		
		dtcm.addColumn(tc1);
		dtcm.addColumn(tc2);
		dtcm.addColumn(tc3);
		dtcm.addColumn(tc4);
		dtcm.addColumn(tc5);
		
		j_table.setBackground(Color.white);
		
		j_table.setGridColor(Color.DARK_GRAY);

		j_table.setShowHorizontalLines(true);
		j_table.setShowVerticalLines(true);
		

		con_assign.setLayout(new BorderLayout());
		con_assign.add("Center", j_table);
		con_assign.add("East", button_panel);
		con_assign.add(new JScrollPane(j_table));
		
	}
	
	
	//다시 만든 다이얼로그
	public void left()
	{
		left_Panel.setBorder(new TitledBorder("일시 및 제목"));
		left_Panel.setLayout(new BorderLayout());
		
		JPanel left_date = new JPanel();
		JPanel left_date1 = new JPanel();
		JPanel left_date2 = new JPanel();
		JPanel subPanel = new JPanel();
		JPanel left_title = new JPanel();
		
		//날짜 불러오기
		form.applyPattern("yyyy");
		year = form.format(cal.getTime());
		form.applyPattern("MM");
		month = form.format(cal.getTime());
		form.applyPattern("dd");
		day = form.format(cal.getTime());
		
		
		//나열하기
		subPanel.setLayout(new BorderLayout());
		//
		left_date1.setBorder(new TitledBorder("시작일시"));
		left_date1.setLayout(new FlowLayout());
		left_date1.add(tf_year);
		left_date1.add(lb_Year);
		left_date1.add(tf_Month);
		left_date1.add(lb_Month);
		left_date1.add(tf_Day);
		left_date1.add(lb_Day);
		
		left_date1.add(bt_today);
		bt_today.setPreferredSize(new Dimension(50, 30));
		bt_today.setMargin(new Insets(0, 0, 0, 0));
		
		left_date1.add(lb_tab);
		left_date1.add(tf_Hour);
		left_date1.add(lb_Hour);
		left_date1.add(tf_Min);
		left_date1.add(lb_Min);
		//
		left_date2.setBorder(new TitledBorder("제 출 일"));
		left_date2.setLayout(new FlowLayout());
		left_date2.add(tf_year2);
		left_date2.add(lb_Year2);
		left_date2.add(tf_Month2);
		left_date2.add(lb_Month2);
		left_date2.add(tf_Day2);
		left_date2.add(lb_Day2);
		
		left_date2.add(lb_tab2);
		
		left_date2.add(tf_Hour2);
		left_date2.add(lb_Hour2);
		left_date2.add(tf_Min2);
		left_date2.add(lb_Min2);
		
		//
		left_date.setLayout(new BorderLayout());
		left_date.add("North", left_date1);
		left_date.add("South", left_date2);
		left_title.setBorder(new TitledBorder("제 목"));
		left_title.add(tf_Schdule);
		
		subPanel.add("North", left_date);
		subPanel.add("South", left_title);
		
		left_Panel.setLayout(new GridLayout(1, 3));
		left_Panel.add(subPanel);
	}
	public void right()
	{
		rightup_Panel.setBorder(new TitledBorder("내용 입력"));
		rightup_Panel.setLayout(new BorderLayout());
		
		JPanel right_add = new JPanel();
		JPanel bt_panel = new JPanel();
		JPanel link_panel = new JPanel();
		JPanel link_bt_panel = new JPanel();
		sp_Memo.setWheelScrollingEnabled(true);
		sp_Memo.setAutoscrolls(true);
		sp_Memo.setPreferredSize(new Dimension(410, 130));
		right_add.add("Center", sp_Memo);
		
		bt_save.setPreferredSize(new Dimension(400, 23 ));
		
		//rightup_Panel.setLayout(new BorderLayout());
		rightup_Panel.setLayout(new BorderLayout());
		rightup_Panel.add("North",right_add);
		rightup_Panel.add("South", link_bt_panel);
		
		link_bt_panel.setLayout(new BorderLayout());
		link_bt_panel.add("North", bt_panel);
		link_bt_panel.add("South", link_panel);
		
		bt_save.setPreferredSize(new Dimension(100, 30));
		bt_cancel.setPreferredSize(new Dimension(100, 30));
		
		bt_panel.setBorder(new TitledBorder(""));
		bt_panel.setLayout(new GridLayout(1,2));
		bt_panel.add(bt_save);
		bt_panel.add(bt_cancel);
		
		link_panel.setBorder(new TitledBorder("진행 상태"));
		link_panel.setLayout(new GridLayout(1,3));
		
		link_panel.add(link_field);
		link_panel.add(link_button);
	}
	public void today()
	{
		tf_year.setText(year);
		tf_Month.setText(month);
		tf_Day.setText(day);
	}
	public void write()
	{
		JPanel total_write = new JPanel();
		total_write.setLayout(new BorderLayout());
		total_write.add("Center", rightup_Panel);
		total_write.add("North", left_Panel);

		dig_Write.setLayout(new GridLayout(1, 2));
		dig_Write.add("view", total_write);
		

		dig_Write.addWindowListener(new WindowAdapter() {
			   public void windowClosing(WindowEvent e) {
				   bt_add.setText("추 가");
				   }
				  });

	}
		
	public void east(){
		button_panel.setLayout(new GridLayout(3,1));
		button_panel.add(bt_add);
		button_panel.add(button2);
		button_panel.add(button3);
	}	
	
    public void file_save(){
		try {
			File file = new File(FILE_PATH,"Assignment");
			BufferedWriter out = new BufferedWriter(new FileWriter(file));
			String str1[] = new String[vData.size()];
			String str = "";
			
			for (int i = 0; i < vData.size() ; i++) {
				str1[i] = (String)vData.elementAt(i);
			}
			
			
			for (int j = 0; j < str1.length ; j++) {
				if ((j+1)%5 == 0 && (j+1) != str1.length) {
					str += str1[j] + "||\n";
				} 
				else {
					str += str1[j] + "||";
				}
			}

			out.write(str, 0, str.length());  
			//out.newLine();
			
			out.close();
			
		}
			catch ( IOException exc) {
			exc.printStackTrace();
		}
		
    }
    
    public void file_load(){
    	File file = new File(FILE_PATH, "Assignment");
    	
		try{
			BufferedReader in = new BufferedReader(new FileReader(file));
			String str = "";
			vData = new Vector();
			while ((str = in.readLine()) != null ) {
				StringTokenizer token = new StringTokenizer(str, "||" + "\n");
				int i = 0;
				String[] str2 = new String[100]; // while문 안에 있어야 한다.
				int size = vData.size();
				
				while (token.hasMoreTokens()) {
					str2[i] = token.nextToken();
					vData.addElement(str2[i]);
					i++;
				}
				
				dtm.addRow(str2);
			}		
			in.close();
			
			dtcr.setHorizontalAlignment(SwingConstants.CENTER);
			TableColumnModel tcm = j_table.getColumnModel();
			for(int j = 0; j < dtm.getColumnCount(); j++)
			{
				tcm.getColumn(j).setCellRenderer(dtcr);
			}
		}catch( IOException e) {
			e.printStackTrace();
		}
	
    }
	
	public void actionPerformed(ActionEvent e){
		if(e.getSource()==bt_add){ // 추가버튼 
			tf_year.setText(year);
			tf_Month.setText(month);
			tf_Day.setText(day);
			tf_Hour.setText("00");
			tf_Min.setText("00");
			tf_Schdule.setText("");
			ta_Memo.setText("");
			tf_year2.setText(year);
			tf_Month2.setText("");
			tf_Day2.setText("");
			tf_Hour2.setText("00");
			tf_Min2.setText("00");
			
			link_field.setText("진  행  중");
			dig_Write.setSize(500, 500);
			dig_Write.setLocation(xpos-300, ypos-150);
			dig_Write.setResizable(false);
			dig_Write.setVisible(true);
			
	  
		}
		else if(e.getSource()==bt_save){ // 저장
			
			if((tf_Month.getText().length() < 2)) {
				tf_Month.setText("0" + tf_Month.getText());
			}
			if((tf_Month2.getText().length() < 2)) {
				tf_Month2.setText("0" + tf_Month2.getText());
			}
			if(tf_Day.getText().length() < 2)	{
				tf_Day.setText("0" + tf_Day.getText());
			}
			if(tf_Day2.getText().length() < 2)	{
				tf_Day2.setText("0" + tf_Day2.getText());
			}
			if(tf_Hour.getText().length() < 2)	{
				tf_Hour.setText("0" + tf_Hour.getText());
			}
			if(tf_Hour2.getText().length() < 2)	{
				tf_Hour2.setText("0" + tf_Hour2.getText());
			}
			if(tf_Min.getText().length() < 2)	{
				tf_Min.setText("0" + tf_Min.getText());
			}
			if(tf_Min2.getText().length() < 2)	{
				tf_Min2.setText("0" + tf_Min2.getText());
			}
			
			daytime1 = tf_year.getText() +"."+ tf_Month.getText() + "." + tf_Day.getText() +
	          "(" + tf_Hour.getText() + ":"+  tf_Min.getText() + ")";
			daytime2 = tf_year2.getText() +"."+ tf_Month2.getText() + "." + tf_Day2.getText() +
	          "(" + tf_Hour2.getText() + ":"+  tf_Min2.getText() + ")";
		    title = tf_Schdule.getText();
		    text = ta_Memo.getText();
		    link = link_field.getText();
		    m_values[0] = title;
		    m_values[1] = daytime1;
		    m_values[2] = daytime2;
		    m_values[3] = text;
		    m_values[4] = link;
		    
		    // 입력 예외처리 부분
	    	if(text.equals("") || tf_year.getText().equals("") || tf_Month.getText().equals("") || tf_Day.getText().equals("") || tf_Hour.getText().equals("") || tf_Min.getText().equals("") || tf_year2.getText().equals("") || tf_Month2.getText().equals("") || tf_Day2.getText().equals("") || tf_Hour2.getText().equals("") || tf_Min2.getText().equals("") || link.equals("") || title.equals("")) {
				JOptionPane.showMessageDialog(this, "제목,날짜와 내용을 입력해 주세요!", "확인", 
						JOptionPane.INFORMATION_MESSAGE);
				return;
			}
	    	if(Integer.parseInt(tf_year.getText()) > 2100 || Integer.parseInt(tf_Month.getText()) > 12 || Integer.parseInt(tf_Day.getText()) > 31 || Integer.parseInt(tf_Hour.getText()) > 25 || Integer.parseInt(tf_Min.getText()) > 60) {
				JOptionPane.showMessageDialog(this, "범위를 초과된 값을 입력하셨습니다.!", "확인", 
						JOptionPane.INFORMATION_MESSAGE);
				return;
	    	}
	    	if(Integer.parseInt(tf_year.getText()) > Integer.parseInt(tf_year2.getText())){
				JOptionPane.showMessageDialog(this, "범위를 초과된 값을 입력하셨습니다.!", "확인", 
						JOptionPane.INFORMATION_MESSAGE);
				return;
	    	}
	    	else{
	    		if(Integer.parseInt(tf_year.getText()) == Integer.parseInt(tf_year2.getText()) && Integer.parseInt(tf_Month.getText()) > Integer.parseInt(tf_Month2.getText())){
					JOptionPane.showMessageDialog(this, "범위를 초과된 값을 입력하셨습니다.!", "확인", 
							JOptionPane.INFORMATION_MESSAGE);
					return;
				}
	    		else{
	    			if(Integer.parseInt(tf_Month.getText()) == Integer.parseInt(tf_Month2.getText()) && Integer.parseInt(tf_Day.getText()) > Integer.parseInt(tf_Day2.getText())){
						JOptionPane.showMessageDialog(this, "범위를 초과된 값을 입력하셨습니다.!", "확인", 
								JOptionPane.INFORMATION_MESSAGE);
						return;
					}
	    			else{
	    				if(Integer.parseInt(tf_Day.getText()) == Integer.parseInt(tf_Day2.getText()) && Integer.parseInt(tf_Hour.getText()) > Integer.parseInt(tf_Hour2.getText())){
							JOptionPane.showMessageDialog(this, "범위를 초과된 값을 입력하셨습니다.!", "확인", 
									JOptionPane.INFORMATION_MESSAGE);
							return;
						}
	    				else{
	    					if(Integer.parseInt(tf_Hour.getText()) == Integer.parseInt(tf_Hour2.getText()) && Integer.parseInt(tf_Min.getText()) > Integer.parseInt(tf_Min2.getText())){
								JOptionPane.showMessageDialog(this, "범위를 초과된 값을 입력하셨습니다.!", "확인", 
										JOptionPane.INFORMATION_MESSAGE);
								return;
							}
	    				}
	    			}
	    		}
	    	}
	    	
	    	if(bt_add.getText() == "변 경"){ // 수정
				for(int i = 0; i < m_values.length; i++) {
					dtm.setValueAt(m_values[i], snum, i); // 테이블 채우고	
					vData.remove(snum*5); // 5개 지우고
				}
				
				bt_add.setText("추 가");
				file_save();
			}else
				dtm.addRow(m_values);
	    	
	    	
	    	for (int i = 0; i < m_values.length; i++ ) {
	    		vData.addElement(m_values[i]);
	    	} 
	    	
	    	dig_Write.setVisible(false);
	    	// 필드 초기화
	    	tf_year.setText("");
			tf_Month.setText("");
			tf_Hour.setText("");
			tf_Min.setText("");
			tf_Schdule.setText("");
			ta_Memo.setText("");
			tf_year2.setText("");
			tf_Month2.setText("");
			tf_Hour2.setText("");
			tf_Min2.setText("");
			
			link_field.setText("");
			bt_add.setText("추 가");
			file_save();
			dig_Write.setVisible(false);
	    }
		else if(e.getSource()==bt_cancel){
			dig_Write.setVisible(false);
		}

		else if (e.getSource() == button2) {//정보 수정
			
			snum = j_table.getSelectedRow();
			if (snum == -1){
				JOptionPane.showMessageDialog(this, "수정하려는 목록을 선택하세요");
				return;
			}
			
			for(int i = 0 ; i < m_values.length; i++) {
				m_values[i] = (String)dtm.getValueAt(snum, i);
			}
	
			StringTokenizer token1 = new StringTokenizer(m_values[1], "." + ":" + "(" + ")");
			String[] str = new String[token1.countTokens()]; // while문 안에 있어야 한다.
			int i = 0;
			while (token1.hasMoreTokens()) {
				str[i] = token1.nextToken();
				i++;
			}
			
			StringTokenizer token2 = new StringTokenizer(m_values[2], "." + ":" + "(" + ")");
			String[] str2 = new String[token2.countTokens()]; // while문 안에 있어야 한다.
			int k = 0;
			while (token2.hasMoreTokens()) {
				str2[k] = token2.nextToken();
				k++;
			}
			
			tf_year.setText(str[0]);
			tf_Month.setText(str[1]);
			tf_Day.setText(str[2]);
			tf_Hour.setText(str[3]);
			tf_Min.setText(str[4]);
			tf_year2.setText(str2[0]);
			tf_Month2.setText(str2[1]);
			tf_Day2.setText(str2[2]);
			tf_Hour2.setText(str2[3]);
			tf_Min2.setText(str2[4]);
			tf_Schdule.setText(m_values[0]);
			ta_Memo.setText(m_values[3]);
			link_field.setText(m_values[4]);
			
			bt_add.setText("변 경");
			dig_Write.setSize(500, 500);
			dig_Write.setLocation(xpos-300, ypos-150);
			dig_Write.setResizable(false);
			dig_Write.setVisible(true);
		}
		else if(e.getSource() == button3)
		{
			int line = j_table.getSelectedRow();
			if (line == -1){
				JOptionPane.showMessageDialog(this, "삭제하려는 목록을 선택하세요");
				return;
			}
			
			dtm.removeRow(line); //선택된 Row 삭제
			
			for(int i = 0; i < 5; i++) {
				vData.remove(line*5);
			}
			file_save();
			return;
		}
		else if (e.getSource() == bt_today)
		{
			today();
		}
		else if (e.getSource() == link_button){
			link_field.setText("완  료");
		}
		
	}
	public String [][] getList(String date)
	{
		
		String [][] retValue = new String [dtm.getRowCount()+1][5];
		String temp = "";
		String tempb = "";
		// 현재 찍은 날짜.
		String a_year = date.substring(0, 4);
		String a_month = date.substring(5, 7);
		String a_day = date.substring(8, 10);
		
		String b_year, b_month, b_day; // 시작 날짜
		String c_year, c_month, c_day; // 종료 날짜
	
		System.out.println(dtm.getRowCount());
		int col = 1;
		int count = 0;
		int [] row = new int [dtm.getRowCount()];
		int [] subdays = new int [dtm.getRowCount()];
		
		for(int i = 0; i < dtm.getRowCount(); i++)
		{
			temp = (String)j_table.getValueAt(i, col);
			
			b_year = temp.substring(0, 4);
			b_month = temp.substring(5, 7);
			b_day = temp.substring(8, 10);
			
			
			tempb = (String)j_table.getValueAt(i, col+1);
			c_year = tempb.substring(0, 4);
			c_month = tempb.substring(5, 7);
			c_day = tempb.substring(8, 10);
			
			if((Integer.parseInt(a_year) >= Integer.parseInt(b_year)) && (Integer.parseInt(a_year) <= Integer.parseInt(c_year))) 
			{
				if((Integer.parseInt(a_month) > Integer.parseInt(b_month)) && (Integer.parseInt(c_month) > Integer.parseInt(a_month))) 
				{
					row[count] = i;
					subdays[count] = (Integer.parseInt(c_month) - Integer.parseInt(a_month)) * 30 - (Integer.parseInt(a_day) - Integer.parseInt(c_day));
					count++;
				}
				else if((Integer.parseInt(a_month) == Integer.parseInt(b_month)) && (Integer.parseInt(c_month) > Integer.parseInt(a_month)))
				{
					if((Integer.parseInt(a_day) >= Integer.parseInt(b_day)))
					{
						row[count] = i;
						subdays[count] = (Integer.parseInt(c_month) - Integer.parseInt(a_month)) * 30 - (Integer.parseInt(a_day) - Integer.parseInt(c_day));
						count++;
					}
				}
				else if((Integer.parseInt(a_month) > Integer.parseInt(b_month)) && (Integer.parseInt(c_month) == Integer.parseInt(a_month)))
				{
					if((Integer.parseInt(c_day) >= Integer.parseInt(a_day)))
					{
						row[count] = i;
						subdays[count] = Integer.parseInt(c_day) - Integer.parseInt(a_day);
						count++;
					}
				}
				else if((Integer.parseInt(a_month) == Integer.parseInt(b_month)) && (Integer.parseInt(c_month) == Integer.parseInt(a_month)))
				{
					if((Integer.parseInt(c_day) >= Integer.parseInt(a_day)) && (Integer.parseInt(a_day) >= Integer.parseInt(b_day)))
					{
						row[count] = i;
						subdays[count] = Integer.parseInt(c_day) - Integer.parseInt(a_day);
						count++;
					}
				}
			}
		}
		if(count == 0){
			retValue[0][0] = "없다";
			return retValue;}
		if(count != 0)
		{
			for(int i = 0 ; i < count ; i++) {
				for(int j = 0 ; j < 4 ; j++) {
					retValue[i][j] = (String)dtm.getValueAt(row[i], j);
				}
				retValue[i][4] = Integer.toString(subdays[i]);
			}
		}

		
		return retValue; 
	}
}

