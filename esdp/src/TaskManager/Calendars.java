package TaskManager;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import java.text.SimpleDateFormat;
import java.util.*;

public class Calendars extends JPanel implements ActionListener, MouseListener{
	
	MainFrame mainF;
	private static final String IMAGE_PATH = "resource/Image/";
	
	private JDialog dig_sche = new JDialog(mainF, "스케쥴 정보", true);
	//
	private JPanel mainPanel = new JPanel();
	private JPanel buttonPanel = new JPanel();
	
	private JPanel panel_list = new JPanel();
	
	private JPanel panel_Sche = new JPanel();
	private JPanel panel_Assign = new JPanel();
	private JPanel panel_Diary = new JPanel();
	
	private JPanel panel_ScheTitle = new JPanel();
	private JPanel panel_ScheDetail = new JPanel();
	private JPanel panel_AssignTitle = new JPanel();

	private JPanel panel_DiaryTitle = new JPanel();
	private JPanel panel_DiaryDetail = new JPanel();
	
	private JTextField text_title = new JTextField();
	private JTextArea text_detail1 = new JTextArea();
	private JScrollPane text_detail = new JScrollPane(text_detail1);
	
	private JTextField text_Diarytitle = new JTextField();
	private JTextArea text_Diarydetail1 = new JTextArea();
	private JScrollPane text_Diarydetail = new JScrollPane(text_Diarydetail1);
	
	private JTextArea text_Assigndetail1 = new JTextArea();
	private JScrollPane text_Assigndetail = new JScrollPane(text_Assigndetail1);
	
	private Font font = new Font("굴림체", Font.BOLD, 12);
	private JButton yearbutton1 = new JButton(new ImageIcon(IMAGE_PATH+"yleft.png"));
	private JButton yearbutton2 = new JButton(new ImageIcon(IMAGE_PATH+"yright.png"));
	private JButton monthbutton1 = new JButton(new ImageIcon(IMAGE_PATH+"yleft.png"));
	private JButton monthbutton2 = new JButton(new ImageIcon(IMAGE_PATH+"yright.png"));
	private JLabel year ;
	private JLabel month ;
	private JTable weeksTable;
	private DefaultTableModel model;
	private String[][] daySu = new String[6][7];
	private String[] ju = {"일", "월", "화", "수", "목", "금", "토"};
	private GregorianCalendar cal, cal2; 
	private int currentYear, currentMonth, currentDay, date;
	
	private SimpleDateFormat form = new SimpleDateFormat();
	private Calendar t_cal = Calendar.getInstance(TimeZone.getTimeZone("Asia/Seoul"));
	private String t_year, t_month, t_day; //오늘 날짜
	
	private JTabbedPane jtp = new JTabbedPane();
	
	private int k = 0;
	
	public Calendars(MainFrame mainframe){
		mainF = mainframe;
		cal = new GregorianCalendar();
		currentYear = cal.get(Calendar.YEAR);
		currentMonth= cal.get(Calendar.MONTH);
		
		action();
		dig_set();
		init();
	}
	public void init(){
		
		calMain();
		initPanel();
		initTable();
		label_calendar();
		initLayout();		
	}
	public void initPanel(){
		this.setLayout(new BorderLayout());
		buttonPanel.setLayout(new FlowLayout());
		mainPanel.setLayout(new BorderLayout());
		
		
	}
	public void initTable(){
		model = new DefaultTableModel(daySu, ju){
			boolean[] canEdit = new boolean[]{false , false, false, false, false, false, false};
			public boolean isCellEditable(int rowIndex, int columnIndex){
				return canEdit[columnIndex];
			}
		};

		weeksTable = new JTable(model);
		weeksTable.setRowHeight(21);
		weeksTable.getTableHeader().setResizingAllowed(false);
		weeksTable.setColumnSelectionAllowed(true);
		weeksTable.setFont(font);
		weeksTable.setSelectionBackground(new Color(251,153,254));
		middleSorting(weeksTable);
		weeksTable.addMouseListener(this);
	}
	public void initLayout(){
		yearbutton1.setPreferredSize(new Dimension(20,20)); 
		yearbutton2.setPreferredSize(new Dimension(20,20)); 
		monthbutton1.setPreferredSize(new Dimension(20,20)); 
		monthbutton2.setPreferredSize(new Dimension(20,20)); 
		buttonPanel.add(yearbutton1);
		buttonPanel.add(year);
		buttonPanel.add(yearbutton2);
		buttonPanel.add(monthbutton1);
		buttonPanel.add(month);
		buttonPanel.add(monthbutton2);
		
		
		year.setFont(new Font("맑은 고딕", font.BOLD, 17));
		month.setFont(new Font("맑은 고딕", font.BOLD, 17));
		mainPanel.add("Center",new JScrollPane(weeksTable));
		this.add("North", buttonPanel);
		this.add("Center",mainPanel);
	}
	
	public void calMain(){
		cal2=new GregorianCalendar(currentYear, currentMonth, 1);
		int lastDay= 1;//현재 날짜를 나타낸다.
  		
  		 // 현재 년, 월의 1일로 세팅한다 ex) 2009년 5월 1일로 세팅
		currentDay = cal2.get(Calendar.DAY_OF_WEEK); // 세팅된 1일의 요일을 구한다. 
		date = cal2.getActualMaximum(Calendar.DATE); // 현재 월이 몇일까지 있는지 구한다.
		int i = 0;
		for(i = 0; i < 6; i++){
			for(int j = 0; j < currentDay-1; j++){ // 첫주 첫번째 요일 전까지는 공백문자로 채움
				daySu[0][j] = "";
			}
			for(int z= 0; z < 7; z++){
				// 그달의 마지막날까지 숫자를 1씩 증가한다. (단 초기 공백문자를 채운만큼 더해줘야함.)
				if(lastDay <= date + (currentDay - 1)){
					daySu[i][z]=String.valueOf(lastDay-(currentDay-1));
					
					lastDay++;
				}
			}
		}
    }
	
	public void label_calendar(){
		year = new JLabel("     "+String.valueOf(currentYear)+" 년     ");  //JLabel(String)
		month = new JLabel("     "+String.valueOf(currentMonth+1)+" 월     ");
		year.setFont(font);
		month.setFont(font);
	}
	
	public void middleSorting(JTable weeksTable){
		TableColumnModel tcm = weeksTable.getColumnModel(); // 정렬할 테이블의 컬럼모델을 가져옴
		
		for(int i = 0 ; i < weeksTable.getColumnCount(); i++ ){
			DefaultTableCellRenderer dtcr = new DefaultTableCellRenderer(); // 디폴트테이블셀렌더러를 생성함
			dtcr.setHorizontalAlignment(SwingConstants.CENTER); // 렌더러의 가로정렬을 센터로
			tcm.getColumn(i).setCellRenderer(dtcr); //컴럼모델에서 컬럼의 갯수만큼 컬럼을 가져와 for 문을 이용하여
													// 각각의 셀렌더러를 아까 생성한 dtcr 에 set 해 줌
			TableColumn column = weeksTable.getColumnModel().getColumn(i);
						
			switch(i){
			case 0:
				dtcr.setForeground(Color.RED);
				dtcr.setBackground(new Color(175,225,251));
				break;
			case 6:
				dtcr.setForeground(Color.BLUE);
				dtcr.setBackground(new Color(225,225,223));
				break;
			}
			column.setCellRenderer(dtcr);
			
		}	
	}
	public void start_Task()
	{
		String s="";
		String infor_Sche = "";
		String infor_Diary = "";
		String infor_Assign = "";
		
		form.applyPattern("yyyy");
		t_year = form.format(t_cal.getTime());
		form.applyPattern("MM");
		t_month = form.format(t_cal.getTime());
		form.applyPattern("dd");
		t_day = form.format(cal.getTime());
		
		s += t_year + "." + t_month + "."+ t_day;

		dig_sche.setTitle(t_year + "년 " + t_month + "월 " + t_day + "일의 정보     <Today 매니저>");
		
		infor_Sche = mainF.getSche().getList(s);
		infor_Diary = mainF.getDiary().getList(s);
		if(infor_Sche.equals("없다") && infor_Diary.equals("없다")){
			JOptionPane.showMessageDialog(this, "오늘은 일정이 없습니다.", "ESDP v1.0", 
					JOptionPane.INFORMATION_MESSAGE);
			return;
		}
		
		dig_dialog(s);
	}
	public void dig_set()
	{
		panel_list.setBorder(new TitledBorder("스 케 쥴"));
		
		try{
		// 스케쥴러
		panel_ScheTitle.setBorder(new TitledBorder("제       목"));
		panel_ScheTitle.setLayout(new BorderLayout());
		
		panel_ScheDetail.setLayout(new BorderLayout());
		panel_ScheDetail.setBorder(new TitledBorder("내       용"));
		// 다이어리
		panel_DiaryTitle.setBorder(new TitledBorder("제       목"));
		panel_DiaryTitle.setLayout(new BorderLayout());
		
		panel_DiaryDetail.setLayout(new BorderLayout());
		panel_DiaryDetail.setBorder(new TitledBorder("내       용"));
		
		// 과제
		panel_AssignTitle.setBorder(new TitledBorder("과제 정보"));
		panel_AssignTitle.setLayout(new GridLayout(1,1));
		
		panel_AssignTitle.add(text_Assigndetail);
		text_Assigndetail.setBackground(Color.white);
		text_Assigndetail1.setEditable(false);
		text_Assigndetail1.setLineWrap(true);
		
		// 스케쥴러 텍스트
		panel_ScheTitle.add("Center", text_title);
		panel_ScheDetail.add("Center", text_detail);
		
		text_detail.setPreferredSize(new Dimension(330,400));
		text_title.setEditable(false);
		text_title.setBackground(Color.white);
		text_detail1.setEditable(false);
		text_detail1.setLineWrap(true);
		
		// 다이어리 텍스트
		panel_DiaryTitle.add("Center", text_Diarytitle);
		panel_DiaryDetail.add("Center", text_Diarydetail);
		
		text_Diarydetail.setPreferredSize(new Dimension(330,400));
		text_Diarytitle.setEditable(false);
		text_Diarytitle.setBackground(Color.white);
		text_Diarydetail1.setEditable(false);
		text_Diarydetail1.setLineWrap(true);
		
		// 스케쥴러 통합
		panel_Sche.setLayout(new BorderLayout());
		panel_Sche.add("North", panel_ScheTitle);
		panel_Sche.add("Center", panel_ScheDetail);
		//	다이어리 통합
		panel_Diary.setLayout(new BorderLayout());
		panel_Diary.add("North", panel_DiaryTitle);
		panel_Diary.add("Center", panel_DiaryDetail);
		//  과제 통합
		panel_Assign.setLayout(new GridLayout());
		panel_Assign.add(panel_AssignTitle);
		
		jtp.addTab("Assignment", panel_Assign);
		jtp.addTab("Schedule", panel_Sche);
		jtp.addTab("Diary", panel_Diary);
		dig_sche.setLayout(new GridLayout(1,1));
		dig_sche.add("view", jtp);
		}catch(Exception e){
			System.out.println("error");
		}
	}
	
	public void dig_Schedule(int k)
	{
		String s="";
		
		if(currentDay != 0){
			s+=currentYear+"."+((currentMonth+1)< 10 ? "0":"")+ (currentMonth+1)+ "."+ ( k < 10 ? "0":"") + k;
		}
		
		dig_sche.setTitle(currentYear + "년 " + (currentMonth+1) + "월 " + k + "일 의 정보");
		dig_dialog(s);
		
	}
	
	public void dig_dialog(String s)
	{
		String infor_Sche = "";
		String infor_Diary = "";
		String infor_Assign[][] = null;
		int AssignCount = 0;
		
		// 넘겨받은 정보를 다시 토큰으로 나누어서 출력
		infor_Sche = mainF.getSche().getList(s);
		infor_Diary = mainF.getDiary().getList(s);
		infor_Assign = mainF.getAssign().getList(s);
		
		for(int q=0; q < infor_Assign.length; q++)
		{
			if(infor_Assign[q][0] != null)
			{
				AssignCount++;
			}
		}
		
		if(infor_Sche.equals("없다") && infor_Diary.equals("없다") && infor_Assign[0][0].equals("없다")){
			JOptionPane.showMessageDialog(this, "해당 날짜에 내용이 없습니다!", "확 인", 
					JOptionPane.INFORMATION_MESSAGE);
			return;
		}
		else{
			StringTokenizer tokenSche = new StringTokenizer(infor_Sche, "||");
			StringTokenizer tokenDiary = new StringTokenizer(infor_Diary, "||");
			String[] strSche = new String[tokenSche.countTokens()];
			String[] strDiary = new String[tokenDiary.countTokens()];
			String temp = "";
			int i = 0;
			int j = 0;
			
			
			while (tokenSche.hasMoreTokens()) {
				strSche[i] = tokenSche.nextToken();
				i++;
			}
			while (tokenDiary.hasMoreTokens()) {
				strDiary[j] = tokenDiary.nextToken();
				j++;
			}
			if(infor_Sche.equals("없다"))
			{
				text_title.setText("금일 입력된 내용이 없습니다!");
				text_detail1.setText(null);
			}
			else{
				text_title.setText(strSche[1]);
				text_detail1.setText(strSche[2]);
			}
			
			
			text_title.setFont(new Font("굴림체", 0, 12));
			text_detail1.setFont(new Font("굴림체", 0, 12));
			
			if(infor_Diary.equals("없다"))
			{
				text_Diarytitle.setText("금일 입력된 내용이 없습니다!");
				text_Diarydetail1.setText(null);
			}
			else{
				text_Diarytitle.setText(strDiary[1]);
				text_Diarydetail1.setText(strDiary[2]);
			}
			
			text_Diarytitle.setFont(new Font("굴림체", 0, 12));
			text_Diarydetail.setFont(new Font("굴림체", 0, 12));
			
			
			if(infor_Assign[0][0].equals("없다"))
			{
				text_Assigndetail1.setText("현재 예정된 과제는 없습니다.");
			}
			else{
				
				for(i=0; i < AssignCount; i++)
				{
					if(Integer.parseInt(infor_Assign[i][4]) == 0)
					{
						temp += "과제 <" + infor_Assign[i][0] + "> 의 제출일 입니다.";
						temp += "\n";
					}
					else{
						temp += "과제 <" + infor_Assign[i][0] + "> 가 " + infor_Assign[i][4] + "일 남았습니다.";
						temp += "\n";
					}
					
				}
				text_Assigndetail1.setText(temp);
			}
			dig_sche.setSize(350, 200);
			Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
			dig_sche.setLocation((int)(screen.getWidth()/2  -  this.getWidth() / 2),(int)(screen.getHeight()/2  -  this.getHeight() / 2));
			dig_sche.setResizable(false);
			dig_sche.setVisible(true);	
		}	
	}
	
	public void action()
	{
		yearbutton1.addActionListener(this);
		yearbutton2.addActionListener(this);
		monthbutton1.addActionListener(this);
		monthbutton2.addActionListener(this);
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		
		if(e.getSource() == yearbutton1){
			currentYear--;
		}else if(e.getSource() == yearbutton2){
			currentYear++;

		}else if(e.getSource() == monthbutton1){
			if(currentMonth > 0){
			currentMonth--;
			}

		}else if(e.getSource() == monthbutton2){
			if(currentMonth < 11){
			currentMonth++;
			}
		}
		for(int i = 0; i < 6; i ++)
			for(int j = 0 ; j < 7 ; j++)
				daySu[i][j]="";
	
		//dig_sche.removeAll();
		mainPanel.removeAll();
		buttonPanel.removeAll();
		weeksTable.removeAll();
		weeksTable.setVisible(false);
		
		this.removeAll();
		this.setVisible(false);
		
		init();
		this.setVisible(true);
	}
	
	@Override
	public void mouseClicked(MouseEvent e) 
	{  
		try{
		
		if(weeksTable.getSelectedRow()>-1&& weeksTable.getSelectedColumn()>-1) 
		{
			k = Integer.parseInt(daySu[weeksTable.getSelectedRow()][weeksTable.getSelectedColumn()]);
			
			dig_Schedule(k);
			
			return;
		}
		}catch (Exception a) {
		}
	}
	@Override
	public void mouseEntered(MouseEvent me) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseExited(MouseEvent me) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mousePressed(MouseEvent me) {
		// TODO Auto-generated method stub
	//	JOptionPane.showMessageDialog(this, "left");
	}
	@Override
	public void mouseReleased(MouseEvent me) {
		// TODO Auto-generated method stub
		
	}
}
