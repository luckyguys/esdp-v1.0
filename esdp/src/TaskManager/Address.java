package TaskManager;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;
import java.io.*;
import java.util.*;

public class Address extends JFrame implements ActionListener, MouseListener{
	public Container con_Addr = this.getContentPane();
	
	private JPanel panel_Left = new JPanel();
	private JPanel panel_Input = new JPanel();
	private JPanel panel_Search = new JPanel();
	private JPanel panel_Sleft = new JPanel();
	private JPanel panel_Sright = new JPanel();
	private JPanel panel_Choose = new JPanel();
	
	//입력모듈
	private JLabel lb_Name = new JLabel("  이 름");
	private JTextField tf_Name = new JTextField(12);
	private JLabel lb_Phone = new JLabel("  전 화");
	private JTextField tf_Phone = new JTextField(12);
	private JLabel lb_Addr = new JLabel("  주 소");
	private JTextField tf_Addr = new JTextField(100);
	private JLabel lb_Memo = new JLabel("  메 모");
	private JTextField tf_Memo = new JTextField(100);
	private JButton bt_Add = new JButton("추 가");
	private JButton bt_Init = new JButton("다시입력");
		
	//주소록 추가, 수정
	private String[] info_temp = new String[4];
	private String name, phone, addr, memo;
	private int snum;
	
	//검색모듈
	private JLabel lb_Search = new JLabel("검색조건 : ");
	private JLabel lb_Word = new JLabel("검색단어 : ");
	private JCheckBox cb_Name = new JCheckBox("이 름");
	private JCheckBox cb_Phone = new JCheckBox("전 화");
	private JTextField tf_Word = new JTextField(10);
	private JButton bt_Search = new JButton("검 색");
	private JTextArea ta_Result = new JTextArea("검색결과");
	private JScrollPane sp_Result = new JScrollPane(ta_Result);
	
	//리스트출력
	private String[] str = {"이 름", "전화번호", "주 소", "메 모"};  
	private DefaultTableModel dtm = new DefaultTableModel(str, 0); //30줄
	private DefaultTableColumnModel dtcm = new DefaultTableColumnModel(); //열너비 조절위해
	private JTable table = new JTable(dtm, dtcm);
	private JScrollPane jsp = new JScrollPane(table);
	private JPanel panel_Right = new JPanel();
	private TableColumn tc1, tc2, tc3, tc4; //열 너비 지정
	private JButton bt_Modi = new JButton("수정하기");
	private JButton bt_Del = new JButton("삭제하기");
	
	//정보 입출력
	private Vector vData = new Vector(); 
	private String user_Id;
	private File dir;
	
	public Address() {
	
		dir = new File("Address//");
		load();
		action();
		init();
		list();
	}
	
	public void init() {
		// 정보입력
		
		panel_Input.setBorder(new TitledBorder("정보입력"));
		panel_Input.setLayout(new BorderLayout());
		JPanel input_lb = new JPanel();
		JPanel input_tf = new JPanel();
		JPanel input_total = new JPanel();

		input_lb.setLayout(new GridLayout(4,1));
		input_lb.setPreferredSize(new Dimension(40, 100));
		input_tf.setPreferredSize(new Dimension(160, 100));
		input_tf.setLayout(new GridLayout(4,1));
		input_lb.add(lb_Name);
		input_tf.add(tf_Name);
		input_lb.add(lb_Phone);
		input_tf.add(tf_Phone);
		input_lb.add(lb_Addr);
		input_tf.add(tf_Addr);
		input_lb.add(lb_Memo);
		input_tf.add(tf_Memo);
		
		input_total.setLayout(new FlowLayout());
		input_total.add(input_lb);
		input_total.add(input_tf);
		
		JPanel input_bt = new JPanel();
		input_bt.setPreferredSize(new Dimension(0, 20));
		//bt_Init.setPreferredSize(new Dimension(90, 20));
		//bt_Add.setPreferredSize(new Dimension(90, 20));
		input_bt.add(bt_Init);
		input_bt.add(bt_Add);
		
		
		panel_Input.add("North", input_total);
		panel_Input.add("Center", input_bt);

		//검색화면
		panel_Search.setBorder(new TitledBorder("검  색"));
		JPanel panel_Top = new JPanel();
		panel_Top.setLayout(new FlowLayout());
		bt_Search.setPreferredSize(new Dimension(70, 25));
		panel_Top.add(tf_Word);
		panel_Top.add(bt_Search);
		panel_Search.add("Center", panel_Top);
		
		
		
		//전체 화면 구성
		//panel_Left.setLayout(new GridLayout(2,1));
		
		panel_Left.setLayout(new BorderLayout());
		panel_Left.setPreferredSize(new Dimension(220, 0));
		panel_Left.add("Center", panel_Input);
		panel_Left.add("South", panel_Search);
		con_Addr.add("West", panel_Left);	
		con_Addr.add("Center", panel_Right);
		
	}
	
	public void list() { //리스트 생성
		JPanel list_Bottom = new JPanel();
		tc1 = new TableColumn(0, 2);
		tc1.setHeaderValue("이 름" );
		tc2 = new TableColumn(1, 40);
		tc2.setHeaderValue("전화번호");
		tc3 = new TableColumn(2, 100);
		tc3.setHeaderValue("주    소");
		tc4 = new TableColumn(3, 20);
		tc4.setHeaderValue("메 모");
		dtcm.addColumn(tc1);
		dtcm.addColumn(tc2);
		dtcm.addColumn(tc3);
		dtcm.addColumn(tc4);

		
		list_Bottom.setLayout(new FlowLayout());
		list_Bottom.add(bt_Modi);
		list_Bottom.add(bt_Del);
		panel_Right.setBorder(new TitledBorder("목록 보기"));
		panel_Right.setLayout(new BorderLayout());
		panel_Right.add("Center", jsp);
		panel_Right.add("South", list_Bottom);
	}
	
	public void action() {
		bt_Del.addActionListener(this);
		bt_Init.addActionListener(this);
		bt_Add.addActionListener(this);
		bt_Modi.addActionListener(this);
		bt_Search.addActionListener(this);
		table.addMouseListener(this);
	}

	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == bt_Del) { //선택 주소정보 삭제
			int line = table.getSelectedRow();
			if (line == -1){
				JOptionPane.showMessageDialog(this, "삭제하려는  목록을 선택하세요");
				return;
			}
			dtm.removeRow(line); //선택된 Row 삭제
			for(int i = 0; i < 4; i++) {
				vData.remove(line*4);
			}
			save();

			
		}else if (e.getSource() == bt_Init) {//필드 초기화
			tf_Addr.setText("");
			tf_Memo.setText("");
			tf_Name.setText("");
			tf_Phone.setText("");

		}else if (e.getSource() == bt_Add) { // 정보입력
			name = tf_Name.getText();
			phone = tf_Phone.getText();
			addr = tf_Addr.getText();
			memo = tf_Memo.getText();
			info_temp[0] = name;
			info_temp[1] = phone;
			info_temp[2] = addr;
			info_temp[3] = memo;
			if(name.equals("") || phone.equals("") || addr.equals("")) {
				JOptionPane.showMessageDialog(this, "이름, 전화번호, 주소를 입력하세요!", "확인", 
						JOptionPane.INFORMATION_MESSAGE);
				return;
			}
			
			if(bt_Add.getText() == "완료"){ //수정
				for(int i = 0; i < info_temp.length; i++) {
					dtm.setValueAt(info_temp[i], snum, i); //테이블 채우고	
					vData.remove(snum*4); // 4개 지우고
				}
				
				bt_Add.setText("추가");
				save(); //자동 목록 저장
			}else 
				dtm.addRow(info_temp);
				
			//입력 값을 벡터에 저장
			for (int i = 0; i < info_temp.length; i++ ) {
				vData.addElement(info_temp[i]);
			}
			save(); //자동 목록 저장
			// 필드 초기화
			tf_Addr.setText("");
			tf_Memo.setText("");
			tf_Name.setText("");
			tf_Phone.setText("");
			
		}else if (e.getSource() == bt_Modi) {//정보 수정
			snum = table.getSelectedRow();
			if (snum == -1){
				JOptionPane.showMessageDialog(this, "수정하려는 목록을 선택하세요");
				return;
			}
			
			for(int i = 0 ; i < info_temp.length; i++) {
				info_temp[i] = (String)dtm.getValueAt(snum, i);
			}
			
			tf_Name.setText(info_temp[0]);
			tf_Phone.setText(info_temp[1]);
			tf_Addr.setText(info_temp[2]);
			tf_Memo.setText(info_temp[3]);

			
			bt_Add.setText("완료");
			
		}else if (e.getSource() == bt_Search) { //정보 검색
			table.clearSelection();
			String[] search = new String[vData.size()];
			for (int i = 0; i < vData.size() ; i++) {
				search[i] = (String)vData.elementAt(i);
			}
			String keyword = tf_Word.getText();
			if(tf_Word.getText().equals("")){
				JOptionPane.showMessageDialog(this, "검색어를 입력해 주세요", "경 고", 
						JOptionPane.INFORMATION_MESSAGE);		
				return;
			}
				
			boolean bool = false;
			int count = 0;
			for(int i = 0; i < search.length; i++) {
				if(keyword.equals("")) {
					ta_Result.setText("검색어를 입력하세요!");
					return;
				}else if(search[i].indexOf(keyword) != -1) {
					table.addRowSelectionInterval((int)i/4, (int)i/4);
					table.setSelectionForeground(Color.red);
					bool = true;
					count++;
				}
			}
			if(bool){
				JOptionPane.showMessageDialog(this, count + "명 찾았습니다!", "검색결과", 
						JOptionPane.INFORMATION_MESSAGE);
			}
			else 
				JOptionPane.showMessageDialog(this, "검색 결과가 없습니다!", "검색결과", 
						JOptionPane.INFORMATION_MESSAGE);			
		}
		
	}

	
	public void save() {//벡터의 내용을 파일로 저장
		try {
			File file = new File("AddrData");
			
			BufferedWriter out = new BufferedWriter(new FileWriter(file));
			String[] str2 = new String[vData.size()];
			String str = "";
			for (int i = 0; i < vData.size() ; i++) {
				str2[i] = (String)vData.elementAt(i);
			}
			for (int j = 0; j < str2.length ; j++) {
				if ((j+1)%4 == 0 && (j+1) != str2.length) {
					str += str2[j] + "||\n";
				} else {
					str += str2[j] + "||";
				}
			} 
			out.write(str, 0, str.length());
			out.newLine();
			out.close();
		}catch ( IOException exc) {
			exc.printStackTrace();
		}
	}
	
	public void load() { //파일로 부터 읽어서 벡터로 저장후 테이블에 출력
		File file = new File("AddrData");
		try{
			if ( ! file.exists() ) {
				file.createNewFile();
			}

			BufferedReader in = new BufferedReader(new FileReader(file));
			String str = "";
			vData = new Vector();
			while ((str = in.readLine()) != null ) {
				StringTokenizer token = new StringTokenizer(str, "||" + "\n");
				int i = 0;
				String[] str2 = new String[15]; // while문 안에 있어야 한다.
				while (token.hasMoreTokens()) {
					str2[i] = token.nextToken();
					vData.addElement(str2[i]);
					i++;
				}

				dtm.addRow(str2);
			} 
			in.close();
		}catch( IOException e) {
			e.printStackTrace();
		}
	}

	public void mouseClicked(MouseEvent e) {
		table.setSelectionForeground(Color.black);		
	}

	public void mouseEntered(MouseEvent arg0) {
	}

	public void mouseExited(MouseEvent arg0) {
	}

	public void mousePressed(MouseEvent arg0) {
	}

	public void mouseReleased(MouseEvent arg0) {
	}

}
