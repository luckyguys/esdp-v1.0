package TaskManager;

import java.io.*;
import java.awt.*;
import javax.swing.*;

import java.awt.event.*;
import javax.swing.border.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.StringTokenizer;
import java.util.TimeZone;

public class Diary extends JFrame implements ActionListener, WindowListener{
	public Container con_Diary = this.getContentPane();
	
	public JDialog dig_Write = new JDialog(this, "일기쓰기", true);
	private JPanel panel_List = new JPanel();
	private JPanel panel_View = new JPanel();
	private JPanel panel_Button1 = new JPanel();
	private JPanel panel_Button2 = new JPanel();
	private JButton bt_Write = new JButton("일기쓰기");
	private JButton bt_Del = new JButton("삭제하기");
	private JButton bt_Refresh = new JButton("새로고침");
	private JButton bt_Modify = new JButton("다시쓰기");
	private JButton bt_Wwrite = new JButton("작성완료");
	private JButton bt_Wcancel = new JButton("취 소");
	private List list = new List();
	private JScrollPane sp_List;  
	private JTextArea ta_View = new JTextArea();
	private JScrollPane sp_View = new JScrollPane(ta_View);
	
	// write diary	
	private JTextField tf_Title = new JTextField();
	private JTextArea ta_Write = new JTextArea();
	private JScrollPane sp_Write = new JScrollPane(ta_Write);
	
	//get current date
	private Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Asia/Seoul"));	
	private SimpleDateFormat form = new SimpleDateFormat();
	private String date = "";
	
	//file input & output
	private File dir;
	private String title = "";

		
	private Dimension screen;
	private int xpos, ypos;
	
	
	public Diary() {
	
		dir = new File("resource/file/Diary");
		listLoad();
		action();
		view();
		list();
		init();
		write();

		
		screen = Toolkit.getDefaultToolkit().getScreenSize();
		xpos = (int)(screen.getWidth() / 2 - this.getWidth() / 2);
		ypos = (int)(screen.getHeight() / 2 - this.getHeight() / 2);
	}
	
	public void list() {
		sp_List.setWheelScrollingEnabled(true);
		panel_List.setBorder(new TitledBorder("일기목록"));
		panel_List.setLayout(new BorderLayout());
		panel_List.add("Center", sp_List);
		panel_Button1.setLayout(new FlowLayout());
		panel_Button1.add(bt_Write);
		panel_Button1.add(bt_Refresh);
		panel_List.add("South", panel_Button1);
		
	}
	
	public void view() { //view & fix diary
		sp_View.setWheelScrollingEnabled(true);
		ta_View.setEditable(false);
		ta_View.setBackground(Color.white);
		ta_View.setLineWrap(true);
		panel_View.setBorder(new TitledBorder("내용보기"));
		panel_View.setLayout(new BorderLayout());
		panel_View.add("Center", sp_View);
		panel_Button2.setLayout(new FlowLayout());
		panel_Button2.add(bt_Modify);
		panel_Button2.add(bt_Del);
		panel_View.add("South", panel_Button2);
	}
	
	public void init() { //whole screen initialization area
		con_Diary.setLayout(new BorderLayout(5, 0));
		con_Diary.add("West", panel_List);
		con_Diary.add("Center", panel_View);
	}

	
	public void write() { //write diary		
		form.applyPattern("yyyy.MM.dd");
		date = form.format(cal.getTime()); // save date
		
		ta_Write.setLineWrap(true);
		sp_Write.setWheelScrollingEnabled(true);
		sp_Write.setAutoscrolls(true);
		
		JPanel jp_Top = new JPanel();
		JPanel jp_Bottom = new JPanel();
		JLabel lb_Head = new JLabel("타이틀", JLabel.CENTER);
		JLabel lb_Title = new JLabel(" 제 목 ");
		jp_Top.setLayout(new BorderLayout());
		jp_Top.add("North", lb_Head);
		jp_Top.add("West", lb_Title);
		jp_Top.add("Center", tf_Title);
		jp_Bottom.add(bt_Wwrite);
		jp_Bottom.add(bt_Wcancel);
	
		dig_Write.setLayout(new CardLayout(5, 5));
		JPanel total = new JPanel();
		total.setLayout(new BorderLayout());
		total.add("North", jp_Top);
		total.add("Center", sp_Write);
		total.add("South", jp_Bottom);
		dig_Write.add("view", total);

	}
	
	public void action() { //specify action listener
		bt_Modify.addActionListener(this);
		bt_Write.addActionListener(this);
		bt_Wwrite.addActionListener(this);
		bt_Wcancel.addActionListener(this);
		bt_Del.addActionListener(this);
		bt_Refresh.addActionListener(this);
		list.addActionListener(this);
		super.addWindowListener(this);

	}
	
	public void listLoad() {	// Refresh diary list
		sp_List = new JScrollPane(list);
		String[] str_list = dir.list();

		if(str_list != null) {
			for(int i = 0; i < str_list.length; i++) {
				list.add(str_list[i]);
			}
		}
	}
	
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == bt_Modify) { // fix
			if(list.getSelectedItem() == null){
				JOptionPane.showMessageDialog(this, "다시 쓰려는 날짜의 일기를 선택해주세요!");
				return;
			}
				date = list.getSelectedItem();
				dig_Write.setTitle(date);
				dig_Write.setSize(300, 400);
				dig_Write.setLocation(xpos - 150, ypos - 150);
				dig_Write.setResizable(false);
				dig_Write.setVisible(true);	
				
				// date that clicked
				
				
		}else if (e.getSource() == bt_Write) { //write diary		
			form.applyPattern("yyyy.MM.dd");
			date = form.format(cal.getTime()); // save date
			dig_Write.setTitle(date);
			
			dig_Write.setSize(300, 400);
			dig_Write.setLocation(xpos - 150, ypos - 150);
			dig_Write.setResizable(false);
			dig_Write.setVisible(true);	
			
			
		}else if(e.getSource() == bt_Wcancel) { //write cencel
			tf_Title.setText("");
			ta_Write.setText("");
			dig_Write.setVisible(false);
				
		}else if(e.getSource() == bt_Wwrite){ //enroll diary
			String title = tf_Title.getText();
			String content = ta_Write.getText();
			if(title == null || title.trim().length() == 0) {
				JOptionPane.showMessageDialog(this, "제목을 입력하세요!", "확인", 
						JOptionPane.INFORMATION_MESSAGE);
				return;
			}else if(content == null || content.trim().length() == 0) {
				JOptionPane.showMessageDialog(this, "내용을 입력해 주세요!", "확인", 
						JOptionPane.INFORMATION_MESSAGE);
				return;
			}
			if(!dir.exists()) {
				dir.mkdir();
			}
			File file = new File(dir, date);
			try{
				PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(file)));
				out.println(title + "||" + ta_Write.getText().trim());
				out.close();
			}catch(IOException ee){}
			
			tf_Title.setText("");
			ta_Write.setText("");
			dig_Write.setVisible(false);
			list.removeAll();
			panel_List.removeAll();
			panel_List.setVisible(false);
			listLoad();
			list();
			panel_List.setVisible(true);
			// set today's date
			
			
			
		}else if(e.getSource() == list) { // click list
			if(bt_Modify.getText() == "수정완료") { //no runs event when fix diary
				return;
			}
			String data = list.getSelectedItem();
			String strTmp = null;
			String f_Input = "";
			String total_Input = "";
			int j = 0;
			
			File file1 = new File(dir, data);	
			BufferedReader br = null;
			if(file1.canRead()) {
				try {
					br = new BufferedReader(new FileReader(file1));
					ta_View.setText("");
					while((strTmp = br.readLine()) != null) {
						f_Input += strTmp + "\n";
					}
					
					StringTokenizer tokenDiary = new StringTokenizer(f_Input, "||");
					String[] strDiary = new String[tokenDiary.countTokens()];
					while (tokenDiary.hasMoreTokens()) {
						strDiary[j] = tokenDiary.nextToken();
						j++;
					}
					
					total_Input += "일  시 : " + data + "\n제  목 : " + strDiary[0] + "\n-------------------" +
							"------------------------------------------------------------------------\n" 
							+ strDiary[1];
					br.close();
				}catch(Exception ee){}
				ta_View.setText("");
				ta_View.append(total_Input);
				
			}			
			
		}else if(e.getSource() == bt_Del) { // delete
			String data = list.getSelectedItem();
			if (data == null) {
				JOptionPane.showMessageDialog(this, "삭제하려는 일기를 선택하세요");
				return;
			}
			File file = new File(dir, data);
			file.delete();
			ta_View.setText("");
			list.removeAll();
			panel_List.removeAll();
			panel_List.setVisible(false);
			listLoad();
			list();
			panel_List.setVisible(true);
			
		}else if(e.getSource() == bt_Refresh) { // refresh
			if(bt_Modify.getText() == "수정완료") {
				return;
			}
			
			list.removeAll();
			panel_List.removeAll();
			panel_List.setVisible(false);
			listLoad();
			list();
			panel_List.setVisible(true);
			
			
		}
	}

	public String getList(String date)
	{
		String retValue = "";
		String temp = "";
		String subTemp = null;
		String totalTemp = "";
		
		String title = "";
		File file1 = new File(dir, date);	
		
		int row = -1;
		for(int i = 0; i < list.getItemCount(); i++)
		{
			temp = list.getItem(i);
			if(temp.indexOf(date) != -1)
			{
				row = i;
				break;
			}
		}
		if(row == -1){
			return "없다";
		}
		title = list.getItem(row);
		
		
		BufferedReader br = null;
		if(file1.canRead()) {
			try {
				br = new BufferedReader(new FileReader(file1));
				ta_View.setText("");
				while((subTemp = br.readLine()) != null) {
					totalTemp += subTemp + "\n";
				}
				
				retValue += title + "||" + totalTemp;
				br.close();
			}catch(Exception ee){}
		}
		return retValue;
	}

	public void windowActivated(WindowEvent e) {
		
	}

	public void windowClosed(WindowEvent e) {
		
		System.exit(0);
		
	}

	public void windowClosing(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void windowDeactivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void windowDeiconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void windowIconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void windowOpened(WindowEvent e) {

		
	}


}
